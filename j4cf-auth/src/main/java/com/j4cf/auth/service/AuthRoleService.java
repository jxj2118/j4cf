package com.j4cf.auth.service;

import com.j4cf.common.base.BaseService;
import com.j4cf.auth.dao.entity.AuthRole;

/**
* @Description: AuthRole Service接口
* @Author: LongRou
* @CreateDate: 2018/4/25.
* @Version: 1.0
**/
public interface AuthRoleService extends BaseService<AuthRole> {

}