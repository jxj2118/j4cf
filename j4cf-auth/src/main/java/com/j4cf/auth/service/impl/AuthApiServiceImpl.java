package com.j4cf.auth.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.j4cf.auth.dao.entity.*;
import com.j4cf.auth.service.*;
import com.j4cf.common.annotation.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/26 10:48
 * @Version: 1.0
 **/
@Service
@Transactional
@BaseService
public class AuthApiServiceImpl implements AuthApiService{
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthApiServiceImpl.class);

    @Autowired
    AuthUserService authUserService;

    @Autowired
    AuthRoleService authRoleService;

    @Autowired
    AuthUserRoleService authUserRoleService;

    @Autowired
    AuthApiService authApiService;

    @Autowired
    AuthRolePermissionService authRolePermissionService;

    @Autowired
    AuthUserPermissionService authUserPermissionService;

    @Autowired
    AuthPermissionService authPermissionService;

    @Autowired
    AuthOrganizationService authOrganizationService;

    @Autowired
    AuthLogService authLogService;

    /**
     * 根据用户id获取所拥有的权限
     * @param authUserId
     * @return
     */
    @Override
    public List<AuthPermission> selectAuthPermissionByAuthUserId(Integer authUserId) {
        // 用户不存在或锁定状态
        AuthUser authUser = authUserService.selectById(authUserId);
        if (null == authUser || 1 == authUser.getLocked()) {
            LOGGER.info("selectAuthPermissionByAuthUserId : authUserId={}", authUserId);
            return null;
        }
        List<AuthUserPermission> authUserPermissionList = authUserPermissionService.selectList(new EntityWrapper<AuthUserPermission>().eq("user_id",authUserId));
        List<Integer> inpermissionIdList = new ArrayList<Integer>();
        inpermissionIdList.add(-1);
        List<Integer> unpermissionIdList = new ArrayList<Integer>();
        unpermissionIdList.add(-1);
        authUserPermissionList.forEach(v ->{
            if (v.getType() == 1){inpermissionIdList.add(v.getPermissionId());}
            else {unpermissionIdList.add(v.getPermissionId());}
        });
        List<AuthUserRole> authUserRoleList = authUserRoleService.selectList(new EntityWrapper<AuthUserRole>().eq("user_id",authUserId));
        List<Integer> roleIdList = new ArrayList<Integer>();
        roleIdList.add(-1);
        authUserRoleList.forEach(v ->{
            roleIdList.add(v.getRoleId());
        });
        List<AuthRolePermission> authRolePermissionList = authRolePermissionService.selectList(new EntityWrapper<AuthRolePermission>().in("role_id",roleIdList));
        authRolePermissionList.forEach(v ->{
            inpermissionIdList.add(v.getPermissionId());
        });

        return authPermissionService.selectList(new EntityWrapper<AuthPermission>().in("permission_id",
                inpermissionIdList).notIn("permission_id",unpermissionIdList).eq("status",1));
    }

    /**
     * 根据用户id获取所拥有的权限
     * @param authUserId
     * @return
     */
    @Override
    @Cacheable(value = "j4cf-auth-user-permission", key = "'selectAuthPermissionByAuthUserId_' + #authUserId")
    public List<AuthPermission> selectAuthPermissionByAuthUserIdByCache(Integer authUserId) {
        LOGGER.info(authUserId + " => 进入查询权限");
        return selectAuthPermissionByAuthUserId(authUserId);
    }

    /**
     * 根据用户id获取所属的角色
     * @param authUserId
     * @return
     */
    @Override
    public List<AuthRole> selectAuthRoleByAuthUserId(Integer authUserId) {
        // 用户不存在或锁定状态
        AuthUser authUser = authUserService.selectById(authUserId);
        if (null == authUser || 1 == authUser.getLocked()) {
            LOGGER.info("selectAuthRoleByAuthUserId : authUserId={}", authUserId);
            return null;
        }
        List<AuthUserRole> authUserRoleList = authUserRoleService.selectList(new EntityWrapper<AuthUserRole>().eq("user_id",authUserId));
        List<Integer> roleIdList = new ArrayList<Integer>();
        authUserRoleList.forEach(v ->{
            roleIdList.add(v.getRoleId());
        });
        roleIdList.add(-1);
        return authRoleService.selectList(new EntityWrapper<AuthRole>().in("role_id",roleIdList));
    }

    /**
     * 根据用户id获取所属的角色
     * @param authUserId
     * @return
     */
    @Override
    @Cacheable(value = "j4cf-auth-user-role", key = "'selectAuthRoleByAuthUserId_' + #authUserId")
    public List<AuthRole> selectAuthRoleByAuthUserIdByCache(Integer authUserId) {
        return selectAuthRoleByAuthUserId(authUserId);
    }

    /**
     * 根据角色id获取所拥有的权限
     * @param authRoleId
     * @return
     */
    @Override
    public List<AuthRolePermission> selectAuthRolePermisstionByAuthRoleId(Integer authRoleId) {
        List<AuthRolePermission> authRolePermissions = authRolePermissionService.selectList(new EntityWrapper<AuthRolePermission>().eq("role_id",authRoleId));
        return authRolePermissions;
    }

    /**
     * 根据用户id获取所拥有的权限
     * @param authUserId
     * @return
     */
    @Override
    public List<AuthUserPermission> selectAuthUserPermissionByAuthUserId(Integer authUserId) {
        List<AuthUserPermission> authUserPermissions = authUserPermissionService.selectList(new EntityWrapper<AuthUserPermission>().eq("user_id",authUserId));
        return authUserPermissions;
    }

    /**
     * 根据username获取AuthUser
     * @param username
     * @return
     */
    @Override
    public AuthUser selectAuthUserByUsername(String username) {
        return authUserService.selectOne(new EntityWrapper<AuthUser>().eq("username",username));
    }
    /**
     * 根据username获取AuthUser
     * @param username
     * @return
     */
    @Override
    @Cacheable(value = "j4cf-auth-user-info", key = "'selectAuthUserByUsername_' + #username")
    public AuthUser selectAuthUserByUsernameByCache(String username) {
        return selectAuthUserByUsername(username);
    }

    /**
     * 写入操作日志
     * @param record
     * @return
     */
    @Override
    public boolean insertAuthLogSelective(AuthLog record) {
        return authLogService.insert(record);
    }

}
