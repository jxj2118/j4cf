package com.j4cf.auth.service.impl;

import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.auth.dao.mapper.AuthRoleMapper;
import com.j4cf.auth.dao.entity.AuthRole;
import com.j4cf.auth.service.AuthRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* @Description: AuthRole Service实现
* @Author: LongRou
* @CreateDate: 2018/4/25.
* @Version: 1.0
**/
@Service
@Transactional
@BaseService
public class AuthRoleServiceImpl extends BaseServiceImpl<AuthRoleMapper, AuthRole> implements AuthRoleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthRoleServiceImpl.class);

}