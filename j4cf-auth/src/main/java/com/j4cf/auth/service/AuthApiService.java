package com.j4cf.auth.service;

import com.j4cf.auth.dao.entity.*;

import java.util.List;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/26 10:42
 * @Version: 1.0
 **/
public interface AuthApiService {
    /**
     * 根据用户id获取所拥有的权限(用户和角色权限合集)
     * @param authUserId
     * @return
     */
    List<AuthPermission> selectAuthPermissionByAuthUserId(Integer authUserId);

    /**
     * 根据用户id获取所拥有的权限(用户和角色权限合集)
     * @param authUserId
     * @return
     */
    List<AuthPermission> selectAuthPermissionByAuthUserIdByCache(Integer authUserId);

    /**
     * 根据用户id获取所属的角色
     * @param authUserId
     * @return
     */
    List<AuthRole> selectAuthRoleByAuthUserId(Integer authUserId);

    /**
     * 根据用户id获取所属的角色
     * @param authUserId
     * @return
     */
    List<AuthRole> selectAuthRoleByAuthUserIdByCache(Integer authUserId);

    /**
     * 根据角色id获取所拥有的权限
     * @param authRoleId
     * @return
     */
    List<AuthRolePermission> selectAuthRolePermisstionByAuthRoleId(Integer authRoleId);

    /**
     * 根据用户id获取所拥有的权限
     * @param authUserId
     * @return
     */
    List<AuthUserPermission> selectAuthUserPermissionByAuthUserId(Integer authUserId);

    /**
     * 根据条件获取组织数据
     * @param authOrganizationExample
     * @return
     */
    //List<AuthOrganization> selectAuthOrganizationByExample(AuthOrganizationExample authOrganizationExample);

    /**
     * 根据username获取AuthUser
     * @param username
     * @return
     */
    AuthUser selectAuthUserByUsername(String username);

    /**
     * 根据username获取AuthUser
     * @param username
     * @return
     */
    AuthUser selectAuthUserByUsernameByCache(String username);

    /**
     * 写入操作日志
     * @param record
     * @return
     */
    boolean insertAuthLogSelective(AuthLog record);

}
