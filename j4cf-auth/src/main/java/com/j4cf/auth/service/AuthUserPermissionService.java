package com.j4cf.auth.service;

import com.j4cf.common.base.BaseService;
import com.j4cf.auth.dao.entity.AuthUserPermission;

/**
* @Description: AuthUserPermission Service接口
* @Author: LongRou
* @CreateDate: 2018/4/25.
* @Version: 1.0
**/
public interface AuthUserPermissionService extends BaseService<AuthUserPermission> {

}