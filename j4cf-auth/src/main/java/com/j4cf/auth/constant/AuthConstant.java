package com.j4cf.auth.constant;

import com.j4cf.common.base.BaseConstants;

/**
 * @Description: AUTH常量
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/26 11:05
 * @Version: 1.0
 **/
public class AuthConstant extends BaseConstants{
    public static final String AUTH_TYPE = "j4cf.auth.type";

}
