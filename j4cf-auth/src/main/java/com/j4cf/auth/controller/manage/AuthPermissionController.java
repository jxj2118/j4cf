package com.j4cf.auth.controller.manage;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthPermission;
import com.j4cf.auth.service.AuthPermissionService;
import com.j4cf.common.base.BaseApiController;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.common.util.MapUtil;
import com.j4cf.common.validator.LengthValidator;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/27 15:04
 * @Version: 1.0
 **/
@RestController
@RequestMapping("manage/AuthPermission")
public class AuthPermissionController extends BaseApiController<AuthPermission,AuthPermissionService> implements BaseApiService<AuthPermission> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthPermissionController.class);

    @Override
    @ApiOperation(value = "新增权限信息",notes = "新增权限信息")
    @RequiresPermissions("auth:permission:add")
    public BaseResult insert(@RequestBody AuthPermission entity) {
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(entity.getName(), new LengthValidator(1, 30, "权限名称"))
                .on(entity.getMethod(), new LengthValidator(1, 10, "请求类型"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        entity.setCtime(System.currentTimeMillis());
        return super.insert(entity);
    }


    @Override
    @ApiOperation(value = "删除权限信息",notes = "删除权限信息")
    @RequiresPermissions("auth:permission:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
        //return super.deleteById(id);
        return null;
    }

    @Override
    @ApiOperation(value = "根据ID修改权限信息",notes = "根据ID修改权限信息")
    @RequiresPermissions("auth:permission:update")
    @CacheEvict(value = "j4cf-auth-user-permission",allEntries=true)
    public BaseResult updateById(@RequestBody AuthPermission entity) {
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(entity.getName(), new LengthValidator(1, 30, "权限名称"))
                .on(entity.getMethod(), new LengthValidator(1, 10, "请求类型"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        return super.updateById(entity);
    }

    @Override
    @RequiresPermissions("auth:permission:read")
    @ApiOperation(value = "根据ID查询权限信息",notes = "根据ID查询权限信息")
    public BaseResult<AuthPermission> selectById(@RequestParam("id") String id) {
        return super.selectById(id);
    }

    @Override
    @RequiresPermissions("auth:permission:read")
    @ApiOperation(value = "查询所有权限信息",notes = "查询所有权限信息")
    public BaseResult<List<AuthPermission>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params);
    }

    @Override
    @RequiresPermissions("auth:permission:read")
    @ApiOperation(value = "查询权限信息分页",notes = "查询权限信息分页")
    public BaseResult<Page<AuthPermission>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

    @ApiOperation(value = "新增权限信息带生成",notes = "新增权限信息带生成")
    @RequestMapping(value = "add/PermissionAuto", method = RequestMethod.POST ,consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequiresPermissions("auth:permission:add")
    public BaseResult insert(@RequestBody Map<String, Object> params) {
        LOGGER.error(params.toString());
        AuthPermission authPermission = new AuthPermission();
        MapUtil.mapToBean(params,authPermission);
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(authPermission.getName(), new LengthValidator(1, 30, "权限名称"))
                .on(authPermission.getMethod(), new LengthValidator(1, 10, "请求类型"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        authPermission.setCtime(System.currentTimeMillis());
        if (null != params.get("auto") && (boolean)params.get("auto") && (authPermission.getType() == 1 || authPermission.getType() == 2)){
            if (service.insertPermissionAndAutoGenPermisson(authPermission)){
                return new BaseResult(BaseResultEnum.SUCCESS,"新增权限成功");
            }else {return new BaseResult(BaseResultEnum.ERROR,"新增权限失败");}
        }
        return super.insert(authPermission);
    }

    @ApiOperation(value = "根据ID删除权限信息Auto",notes = "根据ID删除权限信息Auto")
    @RequestMapping(value = "delete/PermissionAuto", method = RequestMethod.DELETE)
    @RequiresPermissions("auth:permission:delete")
    @CacheEvict(value = "j4cf-auth-user-permission",allEntries=true)
    public BaseResult PermissionAuto(@RequestParam("id") String id) {
        if (service.deletePermissionAndUse(id)){
            return new BaseResult(BaseResultEnum.SUCCESS,"删除权限成功");
        }else {return new BaseResult(BaseResultEnum.ERROR,"删除权限失败");}
    }

}
