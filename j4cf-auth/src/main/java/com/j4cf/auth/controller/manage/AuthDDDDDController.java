package com.j4cf.auth.controller.manage;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseApiController;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.common.validator.LengthValidator;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/27 15:41
 * @Version: 1.0
 **/
//@RestController
//@RequestMapping("manage/AuthDDDDD")
public class AuthDDDDDController {
//extends BaseApiController<AuthDDDDD,AuthDDDDDService> implements BaseApiService<AuthDDDDD> {
//
//        private static final Logger LOGGER = LoggerFactory.getLogger(AuthDDDDDController.class);
//
//        @Override
//        @ApiOperation(value = "新增{模型名称}信息",notes = "新增{模型名称}信息")
//        @RequiresPermissions("auth:ddddd:add")
//        public BaseResult insert(@RequestBody AuthDDDDD entity) {
//            return super.insert(entity);
//        }
//
//        @Override
//        @ApiOperation(value = "根据ID删除{模型名称}信息",notes = "根据ID删除{模型名称}信息")
//        @RequiresPermissions("auth:ddddd:delete")
//        public BaseResult deleteById(@RequestParam("id") String id) {
//            return super.deleteById(id);
//        }
//
//        @Override
//        @ApiOperation(value = "根据ID修改{模型名称}信息",notes = "根据ID修改{模型名称}信息")
//        @RequiresPermissions("auth:ddddd:update")
//        public BaseResult updateById(@RequestBody AuthDDDDD entity) {
//            return super.updateById(entity);
//        }
//
//        @Override
//        @ApiOperation(value = "根据ID查询{模型名称}信息",notes = "根据ID查询{模型名称}信息")
//        @RequiresPermissions("auth:ddddd:read")
//        public BaseResult<AuthDDDDD> selectById(@RequestParam("id") String id) {
//            return super.selectById(id);
//        }
//
//        @Override
//        @ApiOperation(value = "查询所有{模型名称}信息",notes = "查询所有{模型名称}信息")
//        @RequiresPermissions("auth:ddddd:read")
//        public BaseResult<List<AuthDDDDD>> selectAll(@RequestParam Map<String, Object> params) {
//            return super.selectAll(params);
//        }
//
//        @Override
//        @ApiOperation(value = "查询{模型名称}信息分页",notes = "查询{模型名称}信息分页")
//        @RequiresPermissions("auth:ddddd:read")
//        public BaseResult<Page<AuthDDDDD>> selectPage(@RequestParam Map<String, Object> params) {
//            return super.selectPage(params);
//        }
}
