package com.j4cf.auth.controller.manage;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthRole;
import com.j4cf.auth.dao.entity.AuthRolePermission;
import com.j4cf.auth.dao.entity.AuthUserRole;
import com.j4cf.auth.service.AuthApiService;
import com.j4cf.auth.service.AuthRolePermissionService;
import com.j4cf.auth.service.AuthRoleService;
import com.j4cf.auth.service.AuthUserRoleService;
import com.j4cf.common.base.BaseApiController;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.common.validator.LengthValidator;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/27 15:37
 * @Version: 1.0
 **/
@RestController
@RequestMapping("manage/AuthRole")
public class AuthRoleController extends BaseApiController<AuthRole,AuthRoleService> implements BaseApiService<AuthRole> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthRoleController.class);

    @Autowired
    private AuthRolePermissionService authRolePermissionService;
    @Autowired
    private AuthUserRoleService authUserRoleService;
    @Autowired
    private AuthApiService authApiService;

    @Override
    @ApiOperation(value = "新增角色信息",notes = "新增角色信息")
    @RequiresPermissions("auth:role:add")
    public BaseResult insert(@RequestBody AuthRole entity) {
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(entity.getName(), new LengthValidator(1, 30, "角色名称"))
                .on(entity.getTitle(), new LengthValidator(1, 32, "角色标题"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        entity.setCtime(System.currentTimeMillis());
        entity.setOrders(System.currentTimeMillis());
        return super.insert(entity);
    }

    @Override
    @ApiOperation(value = "根据ID删除角色信息",notes = "根据ID删除角色信息")
    @RequiresPermissions("auth:role:delete")
    @CacheEvict(value = "j4cf-auth-user-role",allEntries=true)
    public BaseResult deleteById(@RequestParam("id") String id) {
        return super.deleteById(id);
    }

    @Override
    @ApiOperation(value = "根据ID修改角色信息",notes = "根据ID修改角色信息")
    @RequiresPermissions("auth:role:update")
    @CacheEvict(value = "j4cf-auth-user-role",allEntries=true)
    public BaseResult updateById(@RequestBody AuthRole entity) {
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(entity.getName(), new LengthValidator(1, 30, "角色名称"))
                .on(entity.getTitle(), new LengthValidator(1, 32, "角色标题"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询角色信息",notes = "根据ID查询角色信息")
    @RequiresPermissions("auth:role:read")
    public BaseResult<AuthRole> selectById(@RequestParam("id") String id) {
        return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有角色信息",notes = "查询所有角色信息")
    @RequiresPermissions("auth:role:read")
    public BaseResult<List<AuthRole>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params);
    }

    @Override
    @ApiOperation(value = "查询角色信息分页",notes = "查询角色信息分页")
    @RequiresPermissions("auth:role:read")
    public BaseResult<Page<AuthRole>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }


    @ApiOperation(value = "根据角色id获取所拥有的权限",notes = "根据角色id获取所拥有的权限")
    @RequestMapping(value = "select/getPermissionByRoleId", method = RequestMethod.GET)
    public BaseResult<List<AuthRolePermission>> selectAuthRolePermisstionByAuthRoleId(@RequestParam("id") String id) {
        List<AuthRolePermission> list = authRolePermissionService.selectList(new EntityWrapper<AuthRolePermission>().eq("role_id",id));
        return new BaseResult(BaseResultEnum.SUCCESS,list);
    }

    @ApiOperation(value = "根据用户id获取所拥有的角色",notes = "根据用户id获取所拥有的角色")
    @RequestMapping(value = "select/getRoleByUserId", method = RequestMethod.GET)
    public BaseResult<List<AuthUserRole>> selectAuthRoleByUserId(@RequestParam("id") String id) {
        List<AuthUserRole> list = authUserRoleService.selectList(new EntityWrapper<AuthUserRole>().eq("user_id",id));
        return new BaseResult(BaseResultEnum.SUCCESS,list);
    }
    @ApiOperation(value = "根据用户id获取所拥有的角色",notes = "根据用户id获取所拥有的角色")
    @RequestMapping(value = "select/getRoleByUserIds", method = RequestMethod.GET)
    public BaseResult<List<AuthRole>> selectAuthRoleByUserIds(@RequestParam("id") String id) {
        List<AuthRole> list = authApiService.selectAuthRoleByAuthUserId(Integer.valueOf(id));
        return new BaseResult(BaseResultEnum.SUCCESS,list);
    }

    @ApiOperation(value = "为角色添加权限",notes = "为角色添加权限")
    @RequestMapping(value = "add/addPermissionByRoleId", method = RequestMethod.POST ,consumes = MediaType.APPLICATION_JSON_VALUE)
    @CacheEvict(value = "j4cf-auth-user-role",allEntries=true)
    public BaseResult insertPermissionByRoleId(@RequestBody Map<String, Object> params) {
        LOGGER.error(params.toString());
        AuthRole authRole = service.selectById((Integer)params.get("rodeId"));
        if (null == authRole){return new BaseResult(BaseResultEnum.ERROR,"角色不存在！");}
        List<Integer> list = (List<Integer>) params.get("list");
        LOGGER.error(list.toString());
        if (authRolePermissionService.replacePermissionByRoleId(authRole.getRoleId(),list)){return new BaseResult(BaseResultEnum.SUCCESS,"保存成功");}
        return new BaseResult(BaseResultEnum.ERROR,"更新权限失败");
    }
}
