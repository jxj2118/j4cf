package com.j4cf.auth.controller.manage;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthPermission;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.auth.dao.entity.AuthUserPermission;
import com.j4cf.auth.service.AuthApiService;
import com.j4cf.auth.service.AuthUserPermissionService;
import com.j4cf.auth.service.AuthUserRoleService;
import com.j4cf.auth.service.AuthUserService;
import com.j4cf.common.base.BaseApiController;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.common.util.MD5Util;
import com.j4cf.common.validator.LengthValidator;
import com.j4cf.common.validator.NotNullValidator;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/26 16:50
 * @Version: 1.0
 **/
@RestController
@RequestMapping("manage/AuthUser")
public class AuthUserController extends BaseApiController<AuthUser, AuthUserService> implements BaseApiService<AuthUser> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthUserController.class);

    @Autowired
    private AuthUserPermissionService authUserPermissionService;
    @Autowired
    private AuthUserRoleService authUserRoleService;
    @Autowired
    private AuthApiService authApiService;

    @Override
    @ApiOperation(value = "新增用户信息", notes = "新增用户信息")
    @RequiresPermissions("auth:user:add")
    public BaseResult insert(@RequestBody AuthUser entity) {
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(entity.getUsername(), new LengthValidator(1, 20, "帐号"))
                .on(entity.getPassword(), new LengthValidator(5, 32, "密码"))
                .on(entity.getRealname(), new NotNullValidator("姓名"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        String salt = UUID.randomUUID().toString().replaceAll("-", "");
        entity.setSalt(salt);
        entity.setPassword(MD5Util.md5(entity.getPassword() + entity.getSalt()));
        entity.setCtime(System.currentTimeMillis());
        AuthUser authUser = service.selectOne(new EntityWrapper<AuthUser>().eq("username",entity.getUsername()));
        if (null != authUser){return new BaseResult(BaseResultEnum.ERROR, "账号已存在");}
        return super.insert(entity);
    }

    @Override
    @ApiOperation(value = "根据ID删除用户信息", notes = "根据ID删除用户信息")
    @RequiresPermissions("auth:user:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
        return new BaseResult(BaseResultEnum.ERROR,"暂停删除");
    }

    @Override
    @ApiOperation(value = "根据ID修改用户信息", notes = "根据ID修改用户信息")
    @RequiresPermissions("auth:user:update")
    public BaseResult updateById(@RequestBody AuthUser entity) {
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(entity.getUsername(), new LengthValidator(1, 20, "帐号"))
                .on(entity.getPassword(), new LengthValidator(5, 32, "密码"))
                .on(entity.getRealname(), new NotNullValidator("真实姓名"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        AuthUser authUser = service.selectOne(new EntityWrapper<AuthUser>().eq("username",entity.getUsername()));
        if (null == authUser){return new BaseResult(BaseResultEnum.ERROR, "错误的请求");}
        if (authUser.getUserId().equals(entity.getUserId())){return new BaseResult(BaseResultEnum.ERROR, "错误的请求");}
        if (!entity.getPassword().equals(authUser.getPassword())){entity.setPassword(MD5Util.md5(entity.getPassword() + entity.getSalt()));}
        return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询用户信息", notes = "根据ID查询用户信息")
    @RequiresPermissions("auth:user:read")
    public BaseResult<AuthUser> selectById(@RequestParam("id") String id) {
        return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有用户信息", notes = "查询所有用户信息")
    @RequiresPermissions("auth:user:read")
    public BaseResult<List<AuthUser>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params);
    }

    @Override
    @ApiOperation(value = "查询用户信息分页", notes = "查询用户信息分页")
    @RequiresPermissions("auth:user:read")
    public BaseResult<Page<AuthUser>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

    @ApiOperation(value = "根据用户id获取所拥有的权限",notes = "根据用户id获取所拥有的权限")
    @RequestMapping(value = "select/getPermissionByUserId", method = RequestMethod.GET)
    @RequiresPermissions("auth:user:read")
    public BaseResult<List<AuthPermission>> selectAuthUserPermisstionByAuthUserId(@RequestParam("id") String id) {
        List<AuthPermission> list = authApiService.selectAuthPermissionByAuthUserId(Integer.valueOf(id));
        return new BaseResult(BaseResultEnum.SUCCESS,list);
    }
    @ApiOperation(value = "根据用户id获取所拥有的权限，不包括角色含有的",notes = "根据用户id获取所拥有的权限，不包括角色含有的")
    @RequestMapping(value = "select/getPermissionByUserIdWithOutRole", method = RequestMethod.GET)
    @RequiresPermissions("auth:user:read")
    public BaseResult<List<AuthUserPermission>> getPermissionByUserIdWithOutRole(@RequestParam("id") String id) {
        List<AuthUserPermission> list = authUserPermissionService.selectList(new EntityWrapper<AuthUserPermission>().eq("user_id",id));
        return new BaseResult(BaseResultEnum.SUCCESS,list);
    }

    @ApiOperation(value = "为用户添加权限",notes = "为用户添加权限")
    @RequestMapping(value = "add/addPermissionByUserId", method = RequestMethod.POST ,consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequiresPermissions("auth:user:add")
    public BaseResult insertPermissionByUserId(@RequestBody Map<String, Object> params) {
//        LOGGER.error(params.toString());
//        AuthUser authUser = service.selectById((Integer)params.get("rodeId"));
//        if (null == authUser)return new BaseResult(BaseResultEnum.ERROR,"用户不存在！");
//        List<Integer> list = (List<Integer>) params.get("list");
//        LOGGER.error(list.toString());
//        if (authUserPermissionService.replacePermissionByUserId(authUser.getUserId(),list))return new BaseResult(BaseResultEnum.SUCCESS,"保存成功");
        return new BaseResult(BaseResultEnum.ERROR,"更新权限失败");
    }
    @ApiOperation(value = "为用户添加角色",notes = "为用户添加角色")
    @RequestMapping(value = "update/addRoleByUserId", method = RequestMethod.POST ,consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequiresPermissions("auth:user:update")
    public BaseResult addRoleByUserId(@RequestBody Map<String, Object> params) {
        LOGGER.error(params.toString());
        AuthUser authUser = service.selectById((Serializable) params.get("userId"));
        if (null == authUser){return new BaseResult(BaseResultEnum.ERROR,"用户不存在！");}
        List<Integer> list = (List<Integer>) params.get("list");
        LOGGER.error(list.toString());
        if (authUserRoleService.replaceRoleByRoleId(authUser.getUserId(),list)){return new BaseResult(BaseResultEnum.SUCCESS,"保存成功");}
        return new BaseResult(BaseResultEnum.ERROR,"更新用户角色失败");
    }

}
