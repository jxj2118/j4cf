package com.j4cf.auth.dao.mapper;

import com.j4cf.auth.dao.entity.AuthPermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 权限 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-04-25
 */
public interface AuthPermissionMapper extends BaseMapper<AuthPermission> {

}
