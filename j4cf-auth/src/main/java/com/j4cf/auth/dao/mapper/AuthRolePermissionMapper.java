package com.j4cf.auth.dao.mapper;

import com.j4cf.auth.dao.entity.AuthRolePermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关联表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-04-25
 */
public interface AuthRolePermissionMapper extends BaseMapper<AuthRolePermission> {

}
