package com.j4cf.auth.dao.mapper;

import com.j4cf.auth.dao.entity.AuthUserPermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户权限关联表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-04-25
 */
public interface AuthUserPermissionMapper extends BaseMapper<AuthUserPermission> {

}
