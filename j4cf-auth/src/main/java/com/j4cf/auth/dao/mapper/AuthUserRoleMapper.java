package com.j4cf.auth.dao.mapper;

import com.j4cf.auth.dao.entity.AuthUserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-04-25
 */
public interface AuthUserRoleMapper extends BaseMapper<AuthUserRole> {

}
