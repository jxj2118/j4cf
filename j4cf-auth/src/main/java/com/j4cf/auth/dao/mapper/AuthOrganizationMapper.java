package com.j4cf.auth.dao.mapper;

import com.j4cf.auth.dao.entity.AuthOrganization;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 组织 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-04-25
 */
public interface AuthOrganizationMapper extends BaseMapper<AuthOrganization> {

}
