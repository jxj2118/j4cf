package com.j4cf.auth.dao.mapper;

import com.j4cf.auth.dao.entity.AuthRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-04-25
 */
public interface AuthRoleMapper extends BaseMapper<AuthRole> {

}
