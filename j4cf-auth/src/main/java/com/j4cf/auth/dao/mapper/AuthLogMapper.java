package com.j4cf.auth.dao.mapper;

import com.j4cf.auth.dao.entity.AuthLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 操作日志 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-04-25
 */
public interface AuthLogMapper extends BaseMapper<AuthLog> {

}
