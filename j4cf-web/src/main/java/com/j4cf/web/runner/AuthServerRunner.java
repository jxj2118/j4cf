package com.j4cf.web.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/3/27 14:56
 * @Version: 1.0
 **/
@Component
public class AuthServerRunner implements CommandLineRunner{
    private static final String REDIS_USER_PRI_KEY = "J4SC:AUTH:JWT:PRI";
    private static final String REDIS_USER_PUB_KEY = "J4SC:AUTH:JWT:PUB";
    private static final String REDIS_SERVICE_PRI_KEY = "J4SC:AUTH:CLIENT:PRI";
    private static final String REDIS_SERVICE_PUB_KEY = "J4SC:AUTH:CLIENT:PUB";
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Override
    public void run(String... args) throws Exception {

    }
}
