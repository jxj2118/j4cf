package com.j4cf.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/25 17:18
 * @Version: 1.0
 **/
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.j4cf"})//多包扫描
@EnableScheduling
public class J4cfApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(J4cfApplication.class);

    public static void main(String[] args) {
        LOGGER.info(">>>>>J4cf-Web-Server 开始启动>>>>>");

        SpringApplication.run(J4cfApplication.class, args);

        LOGGER.info(">>>>>J4cf-Web-Server 启动完成>>>>>");
    }
}
