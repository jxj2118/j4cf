package com.j4cf.web.controller;

import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/5/23 14:57
 * @Version: 1.0
 **/
@Controller
@RequestMapping
public class HtmlController {
    @RequestMapping(value = "/sys", method = RequestMethod.GET)
    public String test(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
        return "sysindex";
    }
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
        if (request.getServerName().equals("hys.yskxg.com")){
            //管理
            return "sysindex";
        }else if (request.getServerName().equals("hy.yskxg.com")){
            //用户
            return "uindex";
        }
        return "";
    }
}
