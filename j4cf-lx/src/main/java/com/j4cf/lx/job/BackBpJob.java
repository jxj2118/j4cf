package com.j4cf.lx.job;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.lx.controller.LxGoodsCatsController;
import com.j4cf.lx.dao.entity.LxAreaMemberBp;
import com.j4cf.lx.dao.entity.LxBpRecord;
import com.j4cf.lx.dao.entity.LxMemberCard;
import com.j4cf.lx.dao.entity.LxUser;
import com.j4cf.lx.service.LxAreaMemberBpService;
import com.j4cf.lx.service.LxBpRecordService;
import com.j4cf.lx.service.LxMemberCardService;
import com.j4cf.lx.service.LxUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description: 定时返积分任务
 * @Author: LongRou
 * @CreateDate: 2018 2018/5/30 11:58
 * @Version: 1.0
 **/
@Component
public class BackBpJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackBpJob.class);

    @Autowired
    private LxAreaMemberBpService lxAreaMemberBpService;
    @Autowired
    private LxUserService lxUserService;
    @Autowired
    private LxBpRecordService lxBpRecordService;
    @Autowired
    private LxMemberCardService lxMemberCardService;
    /**
     * 每天3点15分发放积分
     */
    //@Scheduled(cron = "0 0/1 * * * ?")
    @Scheduled(cron = "0 15 3 ? * *")
    public void dailyRebate(){
        int page = 1;
        int size = 10;
        Page<LxAreaMemberBp> lxAreaMemberBpPage = new Page<>(page,size);
        do {
            lxAreaMemberBpPage = lxAreaMemberBpService.selectPage(lxAreaMemberBpPage);
            System.out.println(page+"- now time:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            lxAreaMemberBpPage.getRecords().forEach(lxAreaMemberBp -> {
                System.out.println(lxAreaMemberBp);
                // 区域下 还有返利积分的会员
                int userCount = lxUserService.selectCount(new EntityWrapper<LxUser>().eq("area_id",lxAreaMemberBp.getAreaId())
                        .eq("member_type_id",lxAreaMemberBp.getMemberTypeId())
                        .gt("bp_back",0));
                if (userCount == 0){
                    System.out.println(lxAreaMemberBp.getAreaName()+"共有-"+lxAreaMemberBp.getMemberName()+"-会员"+userCount+"人,人数不足无法进行返利。");
                    return;
                }
                int backBp = lxAreaMemberBp.getBackBp() / userCount;
                if (backBp <= 0){
                    System.out.println(lxAreaMemberBp.getAreaName()+"共有-"+lxAreaMemberBp.getMemberName()+"-会员"+userCount+"人,均摊"+lxAreaMemberBp.getBackBp()
                            +"积分发放，人均返利"+backBp+",无法进行返利。");
                    return;
                }
                int userPage = 1;
                Page<LxUser> lxUserPage = new Page<>(userPage,size);
                // total[0] 返利人数
                int[] total = {0};
                do {
                    lxUserPage = lxUserService.selectPage(lxUserPage,new EntityWrapper<LxUser>().eq("area_id",lxAreaMemberBp.getAreaId())
                            .eq("member_type_id",lxAreaMemberBp.getMemberTypeId())
                            .gt("bp_back",0));
                    lxUserPage.getRecords().forEach(lxUser -> {
                        if (lxUser.getBpBack()>= backBp && lxUser.getBpHistory() <= lxUser.getBpMax()){
                            LxBpRecord lxBpRecord = new LxBpRecord();
                            lxBpRecord.setBp(backBp);
                            lxBpRecord.setUserId(lxUser.getUserId());
                            lxBpRecord.setType(1);
                            lxBpRecord.setContent("系统每日返利积分");
                            lxBpRecordService.insert(lxBpRecord);
                            total[0]++;
                        }else {
                            // 积分不足者操作
                            LxBpRecord lxBpRecord = new LxBpRecord();
                            lxBpRecord.setBp(0);
                            lxBpRecord.setUserId(lxUser.getUserId());
                            lxBpRecord.setType(1);
                            lxBpRecord.setContent("系统每日返利积分失败，待返积分不足以获得本次积分或累计积分已达最大");
                            lxBpRecordService.insert(lxBpRecord);
                        }
                    });
                    userPage++;
                    lxUserPage.setCurrent(userPage);
                }while (lxUserPage.getCurrent() <= lxUserPage.getPages());
                System.out.println(lxAreaMemberBp.getAreaName()+"共有-"+lxAreaMemberBp.getMemberName()+"-会员"+userCount+"人，共"+total[0]+
                        "人返利成功，本次总计返利"+(total[0]*backBp)+"分");
            });
            page++;
            lxAreaMemberBpPage.setCurrent(page);
        }while (lxAreaMemberBpPage.getCurrent() <= lxAreaMemberBpPage.getPages());
    }

    /**
     * 每天1点 过期会员卡
     */
    @Scheduled(cron = "0 0 1 * * ?")
    // @Scheduled(cron = "0 0/1 * * * ?")
    public void memberCardEx(){
        int page = 1;
        int size = 10;
        Page<LxMemberCard> lxMemberCardPage = new Page<>(page,size);
        do {
            lxMemberCardPage = lxMemberCardService.selectPage(lxMemberCardPage,new EntityWrapper<LxMemberCard>().lt("otime",System.currentTimeMillis()).eq("state",0));
            if (null != lxMemberCardPage.getRecords() && lxMemberCardPage.getRecords().size()> 0){
                lxMemberCardPage.getRecords().forEach(lxMemberCard -> {
                    lxMemberCard.setState(-1);
                });
                lxMemberCardService.updateBatchById(lxMemberCardPage.getRecords());
            }
            page++;
            lxMemberCardPage.setCurrent(page);
        }while (lxMemberCardPage.getCurrent() <= lxMemberCardPage.getPages());
    }
}
