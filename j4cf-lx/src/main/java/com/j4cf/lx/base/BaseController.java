package com.j4cf.lx.base;

import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.auth.service.AuthApiService;
import com.j4cf.auth.service.AuthUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018-05-13 14:03
 * @Version: 1.0
 **/
public class BaseController extends com.j4cf.common.base.BaseController{
    @Autowired
    AuthApiService authApiService;

    public Integer getUserId(){
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();
        return getUserIdByUsername(username);
    }

    @Cacheable(value = "j4cf-auth-user-info", key = "'getUserIdByUsername_' + #username")
    public Integer getUserIdByUsername(String username){
        AuthUser authUser = authApiService.selectAuthUserByUsername(username);
        return authUser.getUserId();
    }
}
