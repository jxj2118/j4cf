package com.j4cf.lx.service.impl;

import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.mapper.LxAreaMapper;
import com.j4cf.lx.dao.entity.LxArea;
import com.j4cf.lx.service.LxAreaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* @Description: LxArea Service实现
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
@Service
@Transactional
@BaseService
public class LxAreaServiceImpl extends BaseServiceImpl<LxAreaMapper, LxArea> implements LxAreaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxAreaServiceImpl.class);

}