package com.j4cf.lx.service.impl;

import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.mapper.LxAreaMemberBpMapper;
import com.j4cf.lx.dao.entity.LxAreaMemberBp;
import com.j4cf.lx.service.LxAreaMemberBpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* @Description: LxAreaMemberBp Service实现
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
@Service
@Transactional
@BaseService
public class LxAreaMemberBpServiceImpl extends BaseServiceImpl<LxAreaMemberBpMapper, LxAreaMemberBp> implements LxAreaMemberBpService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxAreaMemberBpServiceImpl.class);

}