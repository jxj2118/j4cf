package com.j4cf.lx.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxGoods;
import com.j4cf.lx.dao.entity.LxGoodsCats;

import java.io.Serializable;

/**
* @Description: LxGoods Service接口
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
public interface LxGoodsService extends BaseService<LxGoods> {
    /**
     * 添加商品并绑定分类
     * @param lxGoods
     * @param id
     * @return
     */
    boolean insertAndCats(LxGoods lxGoods,Integer id);

    /**
     * 根据分类查询商品
     * @param tPage
     * @param wrapper
     * @return
     */
    Page<LxGoods> selectPageByCatId(Page<LxGoodsCats> tPage, Wrapper<LxGoodsCats> wrapper);
}