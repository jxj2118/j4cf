package com.j4cf.lx.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxMemberCard;
import com.j4cf.lx.dao.dto.LxMemberDTO;

/**
* @Description: LxMemberCard Service接口
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
public interface LxMemberCardService extends BaseService<LxMemberCard> {
    /**
     * 会员对象关联查询
     * @param page
     * @param wrapper
     * @return
     */
    Page<LxMemberDTO> selectLxMemberPage(Page<LxMemberCard> page, Wrapper<LxMemberCard> wrapper)throws Exception;
}