package com.j4cf.lx.service;

import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxArea;

/**
* @Description: LxArea Service接口
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
public interface LxAreaService extends BaseService<LxArea> {

}