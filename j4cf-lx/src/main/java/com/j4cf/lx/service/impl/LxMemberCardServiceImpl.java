package com.j4cf.lx.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.auth.service.AuthUserService;
import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.dto.LxMemberDTO;
import com.j4cf.lx.dao.entity.LxUser;
import com.j4cf.lx.dao.mapper.LxMemberCardMapper;
import com.j4cf.lx.dao.entity.LxMemberCard;
import com.j4cf.lx.service.LxMemberCardService;
import com.j4cf.lx.service.LxUserService;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: LxMemberCard Service实现
 * @Author: LongRou
 * @CreateDate: 2018/5/8.
 * @Version: 1.0
 **/
@Service
@Transactional
@BaseService
public class LxMemberCardServiceImpl extends BaseServiceImpl<LxMemberCardMapper, LxMemberCard> implements LxMemberCardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxMemberCardServiceImpl.class);

    @Autowired
    AuthUserService authUserService;
    @Autowired
    LxUserService lxUserService;

    @Override
    public Page<LxMemberDTO> selectLxMemberPage(Page<LxMemberCard> page, Wrapper<LxMemberCard> wrapper) throws Exception {
        Page<LxMemberCard> lxMemberCardPage = selectPage(page, wrapper);
        List<Integer> userIdList = new ArrayList<Integer>();
        userIdList.add(-1);
        lxMemberCardPage.getRecords().forEach(lxMemberCard -> {
            if (lxMemberCard.getState() == 1){
                userIdList.add(lxMemberCard.getUserId());
            }
        });
        List<AuthUser> authUserList = authUserService.selectList(new EntityWrapper<AuthUser>().in("user_id", userIdList));
        List<LxUser> lxUserList = lxUserService.selectList(new EntityWrapper<LxUser>().in("user_id", userIdList));
        Page<LxMemberDTO> lxMemberDTOPage = new Page<>();
        List<LxMemberDTO> lxMemberDTOList = new ArrayList<>();
        lxMemberCardPage.getRecords().forEach(lxMemberCard -> {
            LxMemberDTO lxMemberDTO = new LxMemberDTO();
            try {
                authUserList.forEach(authUser -> {
                    if (authUser.getUserId().equals(lxMemberCard.getUserId())) {
                        try {
                            PropertyUtils.copyProperties(lxMemberDTO, authUser);
                        } catch (Exception e) {

                        }
                    }
                });
                lxUserList.forEach(lxUser -> {
                    if (lxUser.getUserId().equals(lxMemberCard.getUserId())) {
                        try {
                            PropertyUtils.copyProperties(lxMemberDTO, lxUser);
                        } catch (Exception e) {

                        }
                    }
                });
                PropertyUtils.copyProperties(lxMemberDTO, lxMemberCard);
                lxMemberDTOList.add(lxMemberDTO);
            } catch (Exception e) {

            }
        });
        PropertyUtils.copyProperties(lxMemberDTOPage, lxMemberCardPage);
        lxMemberDTOPage.setTotal(lxMemberCardPage.getTotal());
        lxMemberDTOPage.setRecords(lxMemberDTOList);
        lxMemberDTOPage.setCurrent(lxMemberCardPage.getCurrent());
        return lxMemberDTOPage;
    }

    public static void main(String[] args) throws Exception {
        Page<LxMemberCard> lxMemberCardPage = new Page<>();
        lxMemberCardPage.setCurrent(5);
        List<LxMemberCard> lxMemberCardList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            LxMemberCard l = new LxMemberCard();
            l.setUserId(i);
            l.setBpInit(i);
            lxMemberCardList.add(l);
        }
        lxMemberCardPage.setRecords(lxMemberCardList);
        Page<LxMemberDTO> lxMemberDTOPage = new Page<>();
        PropertyUtils.copyProperties(lxMemberDTOPage, lxMemberCardPage);
        System.out.println(lxMemberCardPage);
        System.out.println(lxMemberDTOPage);
        System.out.println(lxMemberCardPage.getRecords());
        System.out.println(lxMemberDTOPage.getRecords());
    }
}