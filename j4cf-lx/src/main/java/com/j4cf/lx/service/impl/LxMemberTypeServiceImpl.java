package com.j4cf.lx.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.entity.LxArea;
import com.j4cf.lx.dao.entity.LxAreaMemberBp;
import com.j4cf.lx.dao.mapper.LxMemberTypeMapper;
import com.j4cf.lx.dao.entity.LxMemberType;
import com.j4cf.lx.service.LxAreaMemberBpService;
import com.j4cf.lx.service.LxAreaService;
import com.j4cf.lx.service.LxMemberTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @Description: LxMemberType Service实现
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
@Service
@Transactional
@BaseService
public class LxMemberTypeServiceImpl extends BaseServiceImpl<LxMemberTypeMapper, LxMemberType> implements LxMemberTypeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxMemberTypeServiceImpl.class);
    @Autowired
    private LxAreaService lxAreaService;
    @Autowired
    private LxAreaMemberBpService lxAreaMemberBpService;

    @Override
    public boolean insertAutoGenAMB(LxMemberType lxMemberType) {
        List<LxArea> lxAreas = lxAreaService.selectList(new EntityWrapper<LxArea>().eq("area_pid",1));
        if (insert(lxMemberType)){
            List<LxAreaMemberBp> lxAreaMemberBps = new ArrayList<>();
            lxAreas.forEach(lxArea -> {
                LxAreaMemberBp lxAreaMemberBp = new LxAreaMemberBp();
                lxAreaMemberBp.setAreaId(lxArea.getAreaId());
                lxAreaMemberBp.setAreaName(lxArea.getAreaName());
                lxAreaMemberBp.setMemberName(lxMemberType.getName());
                lxAreaMemberBp.setMemberTypeId(lxMemberType.getMemberTypeId());
                lxAreaMemberBps.add(lxAreaMemberBp);
            });
            return lxAreaMemberBpService.insertBatch(lxAreaMemberBps);
        }
        return false;
    }
}