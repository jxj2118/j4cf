package com.j4cf.lx.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxUser;

import java.util.HashMap;
import java.util.List;

/**
* @Description: LxUser Service接口
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
public interface LxUserService extends BaseService<LxUser> {
    /**
     *  查询所有代理商用户
     * @return
     */
    List<AuthUser> selectAllAgentUser(EntityWrapper<AuthUser> entityWrapper);
    /**
     *  查询所有代理商用户
     * @return
     */
    Page<AuthUser> selectPageAgentUser(Page<AuthUser> page, EntityWrapper<AuthUser> entityWrapper);

    /**
     *  查询所有代理商用户
     * @return
     */
    boolean insertAgentUser(AuthUser authUser);

}