package com.j4cf.lx.service.impl;

import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.mapper.LxNoticeMapper;
import com.j4cf.lx.dao.entity.LxNotice;
import com.j4cf.lx.service.LxNoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* @Description: LxNotice Service实现
* @Author: LongRou
* @CreateDate: 2018/5/13.
* @Version: 1.0
**/
@Service
@Transactional
@BaseService
public class LxNoticeServiceImpl extends BaseServiceImpl<LxNoticeMapper, LxNotice> implements LxNoticeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxNoticeServiceImpl.class);

}