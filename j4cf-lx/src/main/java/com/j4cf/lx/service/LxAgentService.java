package com.j4cf.lx.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxGoods;
import com.j4cf.lx.dao.entity.LxGoodsCats;
import com.j4cf.lx.dao.entity.LxMemberCard;
import com.j4cf.lx.dao.entity.LxUser;

/**
* @Description: LxAgent Service接口
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
public interface LxAgentService {
    /**
     * 绑定会员
     * @param authUser
     * @param lxMemberCard
     * @param lxUser
     * @return
     */
    boolean insertMember(AuthUser authUser, LxMemberCard lxMemberCard,LxUser lxUser);
}