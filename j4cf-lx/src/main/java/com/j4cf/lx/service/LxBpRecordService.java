package com.j4cf.lx.service;

import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxBpRecord;
import com.j4cf.lx.dao.entity.LxUser;

/**
* @Description: LxBpRecord Service接口
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
public interface LxBpRecordService extends BaseService<LxBpRecord> {
    /**
     * 初始化用户及其积分记录
     * @param lxUser
     * @return
     */
    boolean initLxUser(LxUser lxUser);
}