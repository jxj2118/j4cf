package com.j4cf.lx.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.mapper.LxCatsMapper;
import com.j4cf.lx.dao.entity.LxCats;
import com.j4cf.lx.service.LxCatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
* @Description: LxCats Service实现
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
@Service
@Transactional
@BaseService
public class LxCatsServiceImpl extends BaseServiceImpl<LxCatsMapper, LxCats> implements LxCatsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxCatsServiceImpl.class);

    @Override
    public boolean deleteAndChiById(Serializable id) {
        if (delete(new EntityWrapper<LxCats>().eq("cat_id",id))){
            return deleteById(id);
        }
        return false;
    }
}