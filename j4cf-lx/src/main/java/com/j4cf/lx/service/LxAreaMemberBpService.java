package com.j4cf.lx.service;

import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxAreaMemberBp;

/**
* @Description: LxAreaMemberBp Service接口
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
public interface LxAreaMemberBpService extends BaseService<LxAreaMemberBp> {

}