package com.j4cf.lx.service;

import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxMemberType;

/**
* @Description: LxMemberType Service接口
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
public interface LxMemberTypeService extends BaseService<LxMemberType> {
    /**
     * 插入会员类型并自动生成省级区域每日返积分记录
     * @param lxMemberType
     * @return
     */
    boolean insertAutoGenAMB(LxMemberType lxMemberType);
}