package com.j4cf.lx.service.impl;

import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.auth.service.AuthUserService;
import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.entity.LxUser;
import com.j4cf.lx.dao.mapper.LxBpRecordMapper;
import com.j4cf.lx.dao.entity.LxBpRecord;
import com.j4cf.lx.service.LxBpRecordService;
import com.j4cf.lx.service.LxUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: LxBpRecord Service实现
 * @Author: LongRou
 * @CreateDate: 2018/5/8.
 * @Version: 1.0
 **/
@Service
@Transactional
@BaseService
public class LxBpRecordServiceImpl extends BaseServiceImpl<LxBpRecordMapper, LxBpRecord> implements LxBpRecordService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxBpRecordServiceImpl.class);

    @Autowired
    private LxUserService lxUserService;
    @Autowired
    private AuthUserService authUserService;

    @Override
    public boolean insert(LxBpRecord entity) {
        LxUser lxUser = lxUserService.selectById(entity.getUserId());
        switch (entity.getType()) {
            case 1:
                //返利增加
                lxUser.setBp(lxUser.getBp() + entity.getBp());
                lxUser.setBpHistory(lxUser.getBpHistory() + entity.getBp());
                lxUser.setBpBack(lxUser.getBpHistory() - entity.getBp());
                break;
            case 2:
                //其他增加
                lxUser.setBp(lxUser.getBp() + entity.getBp());
                lxUser.setBpHistory(lxUser.getBpHistory() + entity.getBp());
                break;
            case -1:
                //减少
                lxUser.setBp(lxUser.getBp() - entity.getBp());
                break;
            default:
                return false;
        }
        lxUserService.updateById(lxUser);
        entity.setBpNow(lxUser.getBp());
        entity.setCtime(System.currentTimeMillis());
        return super.insert(entity);
    }

    @Override
    public boolean initLxUser(LxUser lxUser) {
        AuthUser authUser = authUserService.selectById(lxUser.getUserId());
        LxBpRecord lxBpRecord = new LxBpRecord();
        lxBpRecord.setBpNow(lxUser.getBp());
        lxBpRecord.setBp(lxUser.getBp());
        lxBpRecord.setCtime(System.currentTimeMillis());
        lxBpRecord.setContent("用户-"+authUser.getUsername()+"-初始化");
        lxBpRecord.setType(0);
        lxBpRecord.setUserId(lxUser.getUserId());
        return super.insert(lxBpRecord);
    }
}