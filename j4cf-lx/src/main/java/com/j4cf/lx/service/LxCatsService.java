package com.j4cf.lx.service;

import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxCats;

import java.io.Serializable;

/**
* @Description: LxCats Service接口
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
public interface LxCatsService extends BaseService<LxCats> {
    /**
     * 删除自身和子集
     * @param id
     * @return
     */
    boolean deleteAndChiById(Serializable id);
}