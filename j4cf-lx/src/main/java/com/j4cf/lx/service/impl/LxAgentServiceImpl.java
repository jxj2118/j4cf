package com.j4cf.lx.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.auth.dao.entity.AuthUserRole;
import com.j4cf.auth.service.AuthUserRoleService;
import com.j4cf.auth.service.AuthUserService;
import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.util.MD5Util;
import com.j4cf.lx.dao.entity.LxBpRecord;
import com.j4cf.lx.dao.entity.LxGoods;
import com.j4cf.lx.dao.entity.LxMemberCard;
import com.j4cf.lx.dao.entity.LxUser;
import com.j4cf.lx.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018-05-29 16:04
 * @Version: 1.0
 **/
@Service
@Transactional
@BaseService
public class LxAgentServiceImpl implements LxAgentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxAgentServiceImpl.class);

    @Autowired
    private LxUserService lxUserService;
    @Autowired
    private AuthUserService authUserService;
    @Autowired
    private AuthUserRoleService authUserRoleService;
    @Autowired
    private LxBpRecordService lxBpRecordService;
    @Autowired
    private LxMemberCardService lxMemberCardService;
    @Autowired
    private LxGoodsService lxGoodsService;


    @Override
    public boolean insertMember(AuthUser authUser, LxMemberCard lxMemberCard,LxUser lxUser) {
        String salt = UUID.randomUUID().toString().replaceAll("-", "");
        authUser.setSalt(salt);
        authUser.setPassword(MD5Util.md5(authUser.getPassword() + authUser.getSalt()));
        authUser.setCtime(System.currentTimeMillis());
        authUserService.insert(authUser);
        AuthUserRole authUserRole = new AuthUserRole();
        authUserRole.setUserId(authUser.getUserId());
        authUserRole.setRoleId(3);
        authUserRoleService.insert(authUserRole);
        //更新会员卡
        lxMemberCard.setUserId(authUser.getUserId());
        lxMemberCard.setState(1);
        lxMemberCard.setOtime(System.currentTimeMillis());
        lxMemberCardService.updateById(lxMemberCard);
        //插入LX会员信息
        lxUser.setMemberTypeId(lxMemberCard.getMemberTypeId());
        lxUser.setUserId(authUser.getUserId());
        lxUser.setCtime(System.currentTimeMillis());
        if (lxUser.getRecommendId() != 1){
            //推荐人返利
            LxBpRecord lxBpRecord = new LxBpRecord();
            lxBpRecord.setUserId(lxUser.getRecommendId());
            lxBpRecord.setType(2);
            lxBpRecord.setContent("推荐会员-"+lxMemberCard.getName()+"-"+authUser.getUsername()+"返利");
            lxBpRecord.setBp(lxMemberCard.getBpRecommend());
            lxBpRecordService.insert(lxBpRecord);
        }
        lxUser.setBpMax(lxMemberCard.getBpMax());
        lxUser.setMemberName(lxMemberCard.getName());
        lxUser.setBpBack(lxMemberCard.getBpInit());
        lxUser.setOtime(lxMemberCard.getOtime());
        lxUserService.insert(lxUser);
        //商品记录
        List<LxGoods> goodsList = JSONObject.parseArray(lxMemberCard.getConsume(), LxGoods.class);
        List<String> goodsIdList = new ArrayList<>();
        goodsIdList.add("-1");
        goodsList.forEach(goods ->{
            goodsIdList.add(goods.getGoodsId());
        });
        List<LxGoods> _goodsList = lxGoodsService.selectList(new EntityWrapper<LxGoods>().in("goods_id",goodsIdList));
        goodsList.forEach(goods ->{
            _goodsList.forEach(_goods ->{
                if (goods.getGoodsId().equals(_goods.getGoodsId())){
                    goods.setSold(_goods.getSold() + goods.getSold());
                }
            });
        });
        lxGoodsService.updateBatchById(goodsList);
        //积分记录
        return lxBpRecordService.initLxUser(lxUser);
    }
}
