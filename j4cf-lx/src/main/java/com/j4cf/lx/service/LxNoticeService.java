package com.j4cf.lx.service;

import com.j4cf.common.base.BaseService;
import com.j4cf.lx.dao.entity.LxNotice;

/**
* @Description: LxNotice Service接口
* @Author: LongRou
* @CreateDate: 2018/5/13.
* @Version: 1.0
**/
public interface LxNoticeService extends BaseService<LxNotice> {

}