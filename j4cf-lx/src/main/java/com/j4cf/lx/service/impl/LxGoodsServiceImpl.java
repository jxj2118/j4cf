package com.j4cf.lx.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.entity.LxGoodsCats;
import com.j4cf.lx.dao.mapper.LxGoodsMapper;
import com.j4cf.lx.dao.entity.LxGoods;
import com.j4cf.lx.service.LxGoodsCatsService;
import com.j4cf.lx.service.LxGoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
* @Description: LxGoods Service实现
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
@Service
@Transactional
@BaseService
public class LxGoodsServiceImpl extends BaseServiceImpl<LxGoodsMapper, LxGoods> implements LxGoodsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxGoodsServiceImpl.class);

    @Autowired
    private LxGoodsCatsService lxGoodsCatsService;

    @Override
    public boolean insertAndCats(LxGoods lxGoods, Integer id) {
        if (insert(lxGoods)){
            LxGoodsCats lxGoodsCats = new LxGoodsCats();
            lxGoodsCats.setGoodsId(lxGoods.getGoodsId());
            lxGoodsCats.setCatId(id);
            return lxGoodsCatsService.insert(lxGoodsCats);
        }
        return false;
    }

    @Override
    public Page<LxGoods> selectPageByCatId(Page<LxGoodsCats> tPage, Wrapper<LxGoodsCats> wrapper) {
        Page<LxGoodsCats> lxGoodsCatsPage = lxGoodsCatsService.selectPage(tPage,wrapper);
        List<String> goodsIdList = new ArrayList<String>();
        goodsIdList.add("-1");
        lxGoodsCatsPage.getRecords().forEach(lxGoodsCats -> {
            goodsIdList.add(lxGoodsCats.getGoodsId());
        });
        Page<LxGoods> lxGoodsPage = new Page<LxGoods>(lxGoodsCatsPage.getCurrent(),lxGoodsCatsPage.getSize());
        return selectPage(lxGoodsPage,new EntityWrapper<LxGoods>().in("goods_id", goodsIdList));
    }
}