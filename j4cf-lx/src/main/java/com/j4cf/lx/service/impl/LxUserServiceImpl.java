package com.j4cf.lx.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.auth.dao.entity.AuthUserRole;
import com.j4cf.auth.service.AuthUserRoleService;
import com.j4cf.auth.service.AuthUserService;
import com.j4cf.common.annotation.BaseService;
import com.j4cf.common.base.BaseServiceImpl;
import com.j4cf.lx.dao.mapper.LxUserMapper;
import com.j4cf.lx.dao.entity.LxUser;
import com.j4cf.lx.service.LxUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @Description: LxUser Service实现
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
@Service
@Transactional
@BaseService
public class LxUserServiceImpl extends BaseServiceImpl<LxUserMapper, LxUser> implements LxUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxUserServiceImpl.class);
    @Autowired
    private AuthUserService authUserService;
    @Autowired
    private AuthUserRoleService authUserRoleService;

    @Override
    public List<AuthUser> selectAllAgentUser(EntityWrapper<AuthUser> entityWrapper) {
        List<AuthUserRole> authUserRoleList = authUserRoleService.selectList(new EntityWrapper<AuthUserRole>().eq("role_id",2));
        List<Integer> authuserIdList = new ArrayList<Integer>();
        authuserIdList.add(-1);
        authUserRoleList.forEach(authUserRole -> authuserIdList.add(authUserRole.getUserId()));
        return authUserService.selectList(entityWrapper.in("user_id",authuserIdList));
    }

    @Override
    public Page<AuthUser> selectPageAgentUser(Page<AuthUser> page, EntityWrapper<AuthUser> entityWrapper) {
        List<AuthUserRole> authUserRoleList = authUserRoleService.selectList(new EntityWrapper<AuthUserRole>().eq("role_id",2));
        List<Integer> authuserIdList = new ArrayList<Integer>();
        authuserIdList.add(-1);
        authUserRoleList.forEach(authUserRole -> authuserIdList.add(authUserRole.getUserId()));
        return authUserService.selectPage(page,entityWrapper.in("user_id",authuserIdList));
    }

    @Override
    public boolean insertAgentUser(AuthUser authUser) {
        authUserService.insert(authUser);
        AuthUserRole authUserRole = new AuthUserRole();
        authUserRole.setUserId(authUser.getUserId());
        authUserRole.setRoleId(2);
        return authUserRoleService.insert(authUserRole);
    }

}