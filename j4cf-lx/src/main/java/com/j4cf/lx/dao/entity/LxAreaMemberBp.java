package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 省级-会员每日返积分表
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_area_member_bp")
public class LxAreaMemberBp extends Model<LxAreaMemberBp> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "amb_id", type = IdType.AUTO)
    private Integer ambId;
    /**
     * 会员类型编号
     */
    @TableField("member_type_id")
    private Integer memberTypeId;
    /**
     * 区域Id
     */
    @TableField("area_id")
    private Integer areaId;
    /**
     * 每日返利总积分
     */
    @TableField("back_bp")
    private Integer backBp;
    /**
     * 会员类型名称
     */
    @TableField("member_name")
    private String memberName;
    @TableField("area_name")
    private String areaName;


    public Integer getAmbId() {
        return ambId;
    }

    public void setAmbId(Integer ambId) {
        this.ambId = ambId;
    }

    public Integer getMemberTypeId() {
        return memberTypeId;
    }

    public void setMemberTypeId(Integer memberTypeId) {
        this.memberTypeId = memberTypeId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getBackBp() {
        return backBp;
    }

    public void setBackBp(Integer backBp) {
        this.backBp = backBp;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Override
    protected Serializable pkVal() {
        return this.ambId;
    }

    @Override
    public String toString() {
        return "LxAreaMemberBp{" +
        "ambId=" + ambId +
        ", memberTypeId=" + memberTypeId +
        ", areaId=" + areaId +
        ", backBp=" + backBp +
        ", memberName=" + memberName +
        ", areaName=" + areaName +
        "}";
    }
}
