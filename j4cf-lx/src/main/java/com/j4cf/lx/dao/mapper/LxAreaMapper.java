package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxArea;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxAreaMapper extends BaseMapper<LxArea> {

}
