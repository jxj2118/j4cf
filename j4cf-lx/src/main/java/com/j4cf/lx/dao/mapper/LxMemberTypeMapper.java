package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxMemberType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员类型 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxMemberTypeMapper extends BaseMapper<LxMemberType> {

}
