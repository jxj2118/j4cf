package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户积分记录
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_bp_record")
public class LxBpRecord extends Model<LxBpRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * 记录编号
     */
    @TableId(value = "bp_record_id", type = IdType.AUTO)
    private Integer bpRecordId;
    /**
     * 操作结束后积分数量
     */
    @TableField("bp_now")
    private Integer bpNow;
    /**
     * 记录明细
     */
    private String content;
    /**
     * 创建时间
     */
    private Long ctime;
    /**
     * 记录类型(0:初始化,1:返利增加,2:其他增加,-1:减少)
     */
    private Integer type;
    /**
     * 操作积分数量
     */
    private Integer bp;
    /**
     * 用户编号
     */
    @TableField("user_id")
    private Integer userId;


    public Integer getBpRecordId() {
        return bpRecordId;
    }

    public void setBpRecordId(Integer bpRecordId) {
        this.bpRecordId = bpRecordId;
    }

    public Integer getBpNow() {
        return bpNow;
    }

    public void setBpNow(Integer bpNow) {
        this.bpNow = bpNow;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getBp() {
        return bp;
    }

    public void setBp(Integer bp) {
        this.bp = bp;
    }

    @Override
    protected Serializable pkVal() {
        return this.bpRecordId;
    }

    @Override
    public String toString() {
        return "LxBpRecord{" +
                "bpRecordId=" + bpRecordId +
                ", bpNow=" + bpNow +
                ", content='" + content + '\'' +
                ", ctime=" + ctime +
                ", type=" + type +
                ", bp=" + bp +
                ", userId=" + userId +
                '}';
    }
}
