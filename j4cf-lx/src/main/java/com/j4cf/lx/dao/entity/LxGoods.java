package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * LX商品表
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_goods")
public class LxGoods extends Model<LxGoods> {

    private static final long serialVersionUID = 1L;

    /**
     * 分类编号
     */
    @TableId(value = "goods_id",type = IdType.UUID)
    private String goodsId;
    /**
     * 商品名称
     */
    @TableField("goods_name")
    private String goodsName;
    /**
     * 关键字，方便查询
     */
    private String keywords;
    /**
     * 市场价
     */
    @TableField("market_price")
    private Double marketPrice;
    /**
     * 售价价
     */
    @TableField("shop_price")
    private Double shopPrice;
    /**
     * 简短描述
     */
    @TableField("goods_brief")
    private String goodsBrief;
    /**
     * HTML描述
     */
    @TableField("goods_desc")
    private String goodsDesc;
    /**
     * 商品小图
     */
    @TableField("goods_thumb")
    private String goodsThumb;
    /**
     * 商品多图  , 分割 
     */
    @TableField("goods_imgs")
    private String goodsImgs;
    /**
     * 创建时间
     */
    private Long ctime;
    /**
     * 是否有下级(0:有,1:没有)
     */
    private Integer type;
    /**
     * 排序
     */
    private Integer order;
    /**
     * 已售
     */
    private Integer sold;
    /**
     * 状态(0:正常,1:不显示)
     */
    private Integer state;
    /**
     * 分类ID
     */
    @TableField(exist = false)
    private Integer catId;


    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Double getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(Double shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getGoodsBrief() {
        return goodsBrief;
    }

    public void setGoodsBrief(String goodsBrief) {
        this.goodsBrief = goodsBrief;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public String getGoodsThumb() {
        return goodsThumb;
    }

    public void setGoodsThumb(String goodsThumb) {
        this.goodsThumb = goodsThumb;
    }

    public String getGoodsImgs() {
        return goodsImgs;
    }

    public void setGoodsImgs(String goodsImgs) {
        this.goodsImgs = goodsImgs;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    protected Serializable pkVal() {
        return this.goodsId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    @Override
    public String toString() {
        return "LxGoods{" +
        "goodsId=" + goodsId +
        ", goodsName=" + goodsName +
        ", keywords=" + keywords +
        ", marketPrice=" + marketPrice +
        ", shopPrice=" + shopPrice +
        ", goodsBrief=" + goodsBrief +
        ", goodsDesc=" + goodsDesc +
        ", goodsThumb=" + goodsThumb +
        ", goodsImgs=" + goodsImgs +
        ", ctime=" + ctime +
        ", type=" + type +
        ", order=" + order +
        ", sold=" + sold +
        ", state=" + state +
        "}";
    }
}
