package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * LX系统公告表
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_notice")
public class LxNotice extends Model<LxNotice> {

    private static final long serialVersionUID = 1L;

    /**
     * 公告编号
     */
    @TableId(value = "notice_id", type = IdType.AUTO)
    private Integer noticeId;
    /**
     * 公告名称
     */
    private String title;
    /**
     * 公告内容
     */
    private String content;
    /**
     * 创建时间
     */
    private Long ctime;
    /**
     * 排序
     */
    private Integer order;
    /**
     * 状态(0:正常,1:不显示)
     */
    private Integer state;


    public Integer getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    protected Serializable pkVal() {
        return this.noticeId;
    }

    @Override
    public String toString() {
        return "LxNotice{" +
        "noticeId=" + noticeId +
        ", title=" + title +
        ", content=" + content +
        ", ctime=" + ctime +
        ", order=" + order +
        ", state=" + state +
        "}";
    }
}
