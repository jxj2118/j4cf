package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 会员类型
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_member_type")
public class LxMemberType extends Model<LxMemberType> {

    private static final long serialVersionUID = 1L;

    /**
     * 会员类型编号
     */
    @TableId(value = "member_type_id", type = IdType.AUTO)
    private Integer memberTypeId;
    /**
     * 会员类型名称
     */
    private String name;
    /**
     * 最大累计获得积分
     */
    @TableField("bp_max")
    private Integer bpMax;
    /**
     * 推荐人可以获得的待返积分（返佣积分）
     */
    @TableField("bp_recommend")
    private Integer bpRecommend;
    /**
     * 用户初始待返积分
     */
    @TableField("bp_init")
    private Integer bpInit;
    /**
     * 创建时间
     */
    private Long ctime;


    public Integer getMemberTypeId() {
        return memberTypeId;
    }

    public void setMemberTypeId(Integer memberTypeId) {
        this.memberTypeId = memberTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBpMax() {
        return bpMax;
    }

    public void setBpMax(Integer bpMax) {
        this.bpMax = bpMax;
    }

    public Integer getBpRecommend() {
        return bpRecommend;
    }

    public void setBpRecommend(Integer bpRecommend) {
        this.bpRecommend = bpRecommend;
    }

    public Integer getBpInit() {
        return bpInit;
    }

    public void setBpInit(Integer bpInit) {
        this.bpInit = bpInit;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    @Override
    protected Serializable pkVal() {
        return this.memberTypeId;
    }

    @Override
    public String toString() {
        return "LxMemberType{" +
        "memberTypeId=" + memberTypeId +
        ", name=" + name +
        ", bpMax=" + bpMax +
        ", bpRecommend=" + bpRecommend +
        ", bpInit=" + bpInit +
        ", ctime=" + ctime +
        "}";
    }
}
