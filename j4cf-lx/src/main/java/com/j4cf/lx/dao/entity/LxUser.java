package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * LX用户表
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_user")
public class LxUser extends Model<LxUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    @TableId("user_id")
    private Integer userId;
    /**
     * 推荐人用户编号
     */
    @TableField("recommend_id")
    private Integer recommendId;
    /**
     * 会员卡编号
     */
    @TableField("member_card_id")
    private Integer memberCardId;
    /**
     * 会员类型编号
     */
    @TableField("member_type_id")
    private Integer memberTypeId;
    /**
     * 区域ID
     */
    @TableField("area_id")
    private Integer areaId;
    /**
     * 会员名称（会注明赠送）
     */
    @TableField("member_name")
    private String memberName;
    /**
     * 最大可获得积分
     */
    @TableField("bp_max")
    private Integer bpMax;
    /**
     * 当前积分
     */
    private Integer bp;
    /**
     * 累计积分
     */
    @TableField("bp_history")
    private Integer bpHistory;
    /**
     * 待返积分
     */
    @TableField("bp_back")
    private Integer bpBack;
    /**
     * 创建时间
     */
    private Long ctime;
    /**
     * 过期时间
     */
    private Long otime;
    /**
     * 状态(0:正常,1:绑定,-1:过期)
     */
    private Integer state;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRecommendId() {
        return recommendId;
    }

    public void setRecommendId(Integer recommendId) {
        this.recommendId = recommendId;
    }

    public Integer getMemberCardId() {
        return memberCardId;
    }

    public void setMemberCardId(Integer memberCardId) {
        this.memberCardId = memberCardId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Integer getBpMax() {
        return bpMax;
    }

    public void setBpMax(Integer bpMax) {
        this.bpMax = bpMax;
    }

    public Integer getBp() {
        return bp;
    }

    public void setBp(Integer bp) {
        this.bp = bp;
    }

    public Integer getBpHistory() {
        return bpHistory;
    }

    public void setBpHistory(Integer bpHistory) {
        this.bpHistory = bpHistory;
    }

    public Integer getBpBack() {
        return bpBack;
    }

    public void setBpBack(Integer bpBack) {
        this.bpBack = bpBack;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getOtime() {
        return otime;
    }

    public void setOtime(Long otime) {
        this.otime = otime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "LxUser{" +
                "userId=" + userId +
                ", recommendId=" + recommendId +
                ", memberCardId=" + memberCardId +
                ", memberTypeId=" + memberTypeId +
                ", areaId=" + areaId +
                ", memberName='" + memberName + '\'' +
                ", bpMax=" + bpMax +
                ", bp=" + bp +
                ", bpHistory=" + bpHistory +
                ", bpBack=" + bpBack +
                ", ctime=" + ctime +
                ", otime=" + otime +
                ", state=" + state +
                '}';
    }

    public Integer getMemberTypeId() {
        return memberTypeId;
    }

    public void setMemberTypeId(Integer memberTypeId) {
        this.memberTypeId = memberTypeId;
    }

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

}
