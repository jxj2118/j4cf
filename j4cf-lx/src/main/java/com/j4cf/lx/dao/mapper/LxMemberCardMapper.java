package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxMemberCard;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员卡（用于分配代理商添加会员） Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxMemberCardMapper extends BaseMapper<LxMemberCard> {

}
