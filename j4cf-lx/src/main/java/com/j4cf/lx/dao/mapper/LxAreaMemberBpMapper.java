package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxAreaMemberBp;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 省级-会员每日返积分表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxAreaMemberBpMapper extends BaseMapper<LxAreaMemberBp> {

}
