package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxNotice;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * LX系统公告表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxNoticeMapper extends BaseMapper<LxNotice> {

}
