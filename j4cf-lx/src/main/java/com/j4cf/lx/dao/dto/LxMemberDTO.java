package com.j4cf.lx.dao.dto;

import com.baomidou.mybatisplus.annotations.TableField;

/**
 * @Description: LxMemberDTO DTO (Data Transfer Object)数据传输对象
 * @Author: LongRou
 * @CreateDate: 2018-05-11 23:25
 * @Version: 1.0
 **/
public class LxMemberDTO {
    /**
     * 会员卡编号
     */
    private Integer memberCardId;
    /**
     * 会员类型编号
     */
    private Integer memberTypeId;
    /**
     * 会员卡名称（会注明赠送）
     */
    private String name;
    /**
     * 最大累计获得积分
     */
    private Integer bpMax;
    /**
     * 推荐人可以获得的待返积分（返佣积分）
     */
    private Integer bpRecommend;
    /**
     * 用户初始待返积分
     */
    private Integer bpInit;
    /**
     * 用户最低需要消费
     */
    private Integer consumeMin;
    /**
     * 用户消费JSON(商品ID,名称,价格)
     */
    private String consume;
    /**
     * 创建时间
     */
    private Long ctime;
    /**
     * 过期时间
     */
    private Long otime;
    /**
     * 状态(0:正常,1:绑定,-1:过期)
     */
    private Integer state;
    /**
     * 代理商用户编号
     */
    private Integer agentUserId;
    /**
     * 用户编号
     */
    private Integer userId;
    /**
     * 帐号
     */
    private String username;
    /**
     * 姓名
     */
    private String realname;
    /**
     * 电话
     */
    private String phone;
    /**
     * 状态(0:正常,1:锁定)
     */
    private Integer locked;
    /**
     * 当前积分
     */
    private Integer bp;
    /**
     * 累计积分
     */
    private Integer bpHistory;
    /**
     * 待返积分
     */
    private Integer bpBack;

    @Override
    public String toString() {
        return "LxMemberDTO{" +
                "memberCardId=" + memberCardId +
                ", memberTypeId=" + memberTypeId +
                ", name='" + name + '\'' +
                ", bpMax=" + bpMax +
                ", bpRecommend=" + bpRecommend +
                ", bpInit=" + bpInit +
                ", consumeMin=" + consumeMin +
                ", consume='" + consume + '\'' +
                ", ctime=" + ctime +
                ", otime=" + otime +
                ", state=" + state +
                ", agentUserId=" + agentUserId +
                ", userId=" + userId +
                ", username='" + username + '\'' +
                ", realname='" + realname + '\'' +
                ", phone='" + phone + '\'' +
                ", locked=" + locked +
                ", bp=" + bp +
                ", bpHistory=" + bpHistory +
                ", bpBack=" + bpBack +
                '}';
    }

    public Integer getBp() {
        return bp;
    }

    public void setBp(Integer bp) {
        this.bp = bp;
    }

    public Integer getBpHistory() {
        return bpHistory;
    }

    public void setBpHistory(Integer bpHistory) {
        this.bpHistory = bpHistory;
    }

    public Integer getBpBack() {
        return bpBack;
    }

    public void setBpBack(Integer bpBack) {
        this.bpBack = bpBack;
    }

    public Integer getMemberCardId() {
        return memberCardId;
    }

    public void setMemberCardId(Integer memberCardId) {
        this.memberCardId = memberCardId;
    }

    public Integer getMemberTypeId() {
        return memberTypeId;
    }

    public void setMemberTypeId(Integer memberTypeId) {
        this.memberTypeId = memberTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBpMax() {
        return bpMax;
    }

    public void setBpMax(Integer bpMax) {
        this.bpMax = bpMax;
    }

    public Integer getBpRecommend() {
        return bpRecommend;
    }

    public void setBpRecommend(Integer bpRecommend) {
        this.bpRecommend = bpRecommend;
    }

    public Integer getBpInit() {
        return bpInit;
    }

    public void setBpInit(Integer bpInit) {
        this.bpInit = bpInit;
    }

    public Integer getConsumeMin() {
        return consumeMin;
    }

    public void setConsumeMin(Integer consumeMin) {
        this.consumeMin = consumeMin;
    }

    public String getConsume() {
        return consume;
    }

    public void setConsume(String consume) {
        this.consume = consume;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getOtime() {
        return otime;
    }

    public void setOtime(Long otime) {
        this.otime = otime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getAgentUserId() {
        return agentUserId;
    }

    public void setAgentUserId(Integer agentUserId) {
        this.agentUserId = agentUserId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getLocked() {
        return locked;
    }

    public void setLocked(Integer locked) {
        this.locked = locked;
    }
}
