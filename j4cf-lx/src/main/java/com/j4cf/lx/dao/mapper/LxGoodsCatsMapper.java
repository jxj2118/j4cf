package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxGoodsCats;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * LX商品分类关联表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxGoodsCatsMapper extends BaseMapper<LxGoodsCats> {

}
