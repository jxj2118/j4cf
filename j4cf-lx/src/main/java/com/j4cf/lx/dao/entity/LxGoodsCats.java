package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * LX商品分类关联表
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_goods_cats")
public class LxGoodsCats extends Model<LxGoodsCats> {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "goods_cat_id", type = IdType.AUTO)
    private Integer goodsCatId;
    /**
     * 分类编号
     */
    @TableField("cat_id")
    private Integer catId;
    /**
     * 商品编号
     */
    @TableField("goods_id")
    private String goodsId;


    public Integer getGoodsCatId() {
        return goodsCatId;
    }

    public void setGoodsCatId(Integer goodsCatId) {
        this.goodsCatId = goodsCatId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    @Override
    protected Serializable pkVal() {
        return this.goodsCatId;
    }

    @Override
    public String toString() {
        return "LxGoodsCats{" +
        "goodsCatId=" + goodsCatId +
        ", catId=" + catId +
        ", goodsId=" + goodsId +
        "}";
    }
}
