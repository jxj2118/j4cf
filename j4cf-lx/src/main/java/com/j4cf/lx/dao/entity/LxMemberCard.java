package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 会员卡（用于分配代理商添加会员）
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_member_card")
public class LxMemberCard extends Model<LxMemberCard> {

    private static final long serialVersionUID = 1L;

    /**
     * 会员卡编号
     */
    @TableId(value = "member_card_id", type = IdType.AUTO)
    private Integer memberCardId;
    /**
     * 会员类型编号
     */
    @TableField("member_type_id")
    private Integer memberTypeId;
    /**
     * 会员卡名称（会注明赠送）
     */
    private String name;
    /**
     * 最大累计获得积分
     */
    @TableField("bp_max")
    private Integer bpMax;
    /**
     * 推荐人可以获得的待返积分（返佣积分）
     */
    @TableField("bp_recommend")
    private Integer bpRecommend;
    /**
     * 用户初始待返积分
     */
    @TableField("bp_init")
    private Integer bpInit;
    /**
     * 用户最低需要消费
     */
    @TableField("consume_min")
    private Integer consumeMin;
    /**
     * 用户消费JSON(商品ID,名称,价格)
     */
    private String consume;
    /**
     * 消费额度
     */
    @TableField("consume_price")
    private Double consumePrice;
    /**
     * 创建时间
     */
    private Long ctime;
    /**
     * 过期时间 绑定成功后为绑定时间
     */
    private Long otime;
    /**
     * 状态(0:正常,1:绑定,-1:过期)
     */
    private Integer state;
    /**
     * 代理商用户编号
     */
    @TableField("agent_user_id")
    private Integer agentUserId;
    /**
     * 用户编号
     */
    @TableField("user_id")
    private Integer userId;


    public Integer getMemberCardId() {
        return memberCardId;
    }

    public void setMemberCardId(Integer memberCardId) {
        this.memberCardId = memberCardId;
    }

    public Integer getMemberTypeId() {
        return memberTypeId;
    }

    public void setMemberTypeId(Integer memberTypeId) {
        this.memberTypeId = memberTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBpMax() {
        return bpMax;
    }

    public void setBpMax(Integer bpMax) {
        this.bpMax = bpMax;
    }

    public Integer getBpRecommend() {
        return bpRecommend;
    }

    public void setBpRecommend(Integer bpRecommend) {
        this.bpRecommend = bpRecommend;
    }

    public Integer getBpInit() {
        return bpInit;
    }

    public void setBpInit(Integer bpInit) {
        this.bpInit = bpInit;
    }

    public Integer getConsumeMin() {
        return consumeMin;
    }

    public void setConsumeMin(Integer consumeMin) {
        this.consumeMin = consumeMin;
    }

    public String getConsume() {
        return consume;
    }

    public void setConsume(String consume) {
        this.consume = consume;
    }

    public Double getConsumePrice() {
        return consumePrice;
    }

    public void setConsumePrice(Double consumePrice) {
        this.consumePrice = consumePrice;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getOtime() {
        return otime;
    }

    public void setOtime(Long otime) {
        this.otime = otime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getAgentUserId() {
        return agentUserId;
    }

    public void setAgentUserId(Integer agentUserId) {
        this.agentUserId = agentUserId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    protected Serializable pkVal() {
        return this.memberCardId;
    }

    @Override
    public String toString() {
        return "LxMemberCard{" +
        "memberCardId=" + memberCardId +
        ", memberTypeId=" + memberTypeId +
        ", name=" + name +
        ", bpMax=" + bpMax +
        ", bpRecommend=" + bpRecommend +
        ", bpInit=" + bpInit +
        ", consumeMin=" + consumeMin +
        ", consume=" + consume +
        ", consumePrice=" + consumePrice +
        ", ctime=" + ctime +
        ", otime=" + otime +
        ", state=" + state +
        ", agentUserId=" + agentUserId +
        ", userId=" + userId +
        "}";
    }
}
