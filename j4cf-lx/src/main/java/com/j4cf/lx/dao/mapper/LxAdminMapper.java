package com.j4cf.lx.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/5/30 16:39
 * @Version: 1.0
 **/
@Mapper
public interface LxAdminMapper {
    /**
     * 查询总消费额度
     * @return
     */
    @Select("SELECT sum(consume_price) FROM j4cf.lx_member_card WHERE state = 1;")
    int getTotalConsumePrice();
    /**
     * 查询总消费额度
     * @return
     */
    @Select("SELECT sum(consume_price) FROM j4cf.lx_member_card where otime > #{time} and state = 1;")
    int getTotalDayConsumePrice(Long time);
}
