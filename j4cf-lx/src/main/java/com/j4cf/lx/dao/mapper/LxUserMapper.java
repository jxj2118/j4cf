package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * LX用户表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxUserMapper extends BaseMapper<LxUser> {

}
