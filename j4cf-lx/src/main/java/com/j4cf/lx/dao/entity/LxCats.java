package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * LX商品分类表
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_cats")
public class LxCats extends Model<LxCats> {

    private static final long serialVersionUID = 1L;

    /**
     * 分类编号
     */
    @TableId(value = "cat_id", type = IdType.AUTO)
    private Integer catId;
    /**
     * 上级分类
     */
    private Integer pid;
    /**
     * 分类名称
     */
    private String name;
    /**
     * 关键字，方便查询
     */
    private String keywords;
    /**
     * 创建时间
     */
    private Long ctime;
    /**
     * 是否有下级(0:有,1:没有)
     */
    private Integer type;
    /**
     * 排序
     */
    private Integer order;
    /**
     * 状态(0:正常,1:不显示)
     */
    private Integer state;


    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    protected Serializable pkVal() {
        return this.catId;
    }

    @Override
    public String toString() {
        return "LxCats{" +
        "catId=" + catId +
        ", pid=" + pid +
        ", name=" + name +
        ", keywords=" + keywords +
        ", ctime=" + ctime +
        ", type=" + type +
        ", order=" + order +
        ", state=" + state +
        "}";
    }
}
