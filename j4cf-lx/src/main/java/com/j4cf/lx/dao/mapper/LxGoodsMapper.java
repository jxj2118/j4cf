package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxGoods;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * LX商品表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxGoodsMapper extends BaseMapper<LxGoods> {

}
