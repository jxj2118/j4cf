package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxBpRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户积分记录 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxBpRecordMapper extends BaseMapper<LxBpRecord> {

}
