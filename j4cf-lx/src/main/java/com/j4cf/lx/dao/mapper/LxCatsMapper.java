package com.j4cf.lx.dao.mapper;

import com.j4cf.lx.dao.entity.LxCats;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * LX商品分类表 Mapper 接口
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
public interface LxCatsMapper extends BaseMapper<LxCats> {

}
