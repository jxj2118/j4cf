package com.j4cf.lx.dao.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author LongRou
 * @since 2018-05-21
 */
@TableName("lx_area")
public class LxArea extends Model<LxArea> {

    private static final long serialVersionUID = 1L;

    @TableId("area_id")
    private Integer areaId;
    @TableField("area_code")
    private String areaCode;
    @TableField("area_name")
    private String areaName;
    @TableField("area_pid")
    private Integer areaPid;
    @TableField("area_level")
    private Integer areaLevel;
    @TableField("area_order")
    private Integer areaOrder;
    @TableField("area_name_en")
    private String areaNameEn;
    @TableField("area_shortname_en")
    private String areaShortnameEn;


    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getAreaPid() {
        return areaPid;
    }

    public void setAreaPid(Integer areaPid) {
        this.areaPid = areaPid;
    }

    public Integer getAreaLevel() {
        return areaLevel;
    }

    public void setAreaLevel(Integer areaLevel) {
        this.areaLevel = areaLevel;
    }

    public Integer getAreaOrder() {
        return areaOrder;
    }

    public void setAreaOrder(Integer areaOrder) {
        this.areaOrder = areaOrder;
    }

    public String getAreaNameEn() {
        return areaNameEn;
    }

    public void setAreaNameEn(String areaNameEn) {
        this.areaNameEn = areaNameEn;
    }

    public String getAreaShortnameEn() {
        return areaShortnameEn;
    }

    public void setAreaShortnameEn(String areaShortnameEn) {
        this.areaShortnameEn = areaShortnameEn;
    }

    @Override
    protected Serializable pkVal() {
        return this.areaId;
    }

    @Override
    public String toString() {
        return "LxArea{" +
        "areaId=" + areaId +
        ", areaCode=" + areaCode +
        ", areaName=" + areaName +
        ", areaPid=" + areaPid +
        ", areaLevel=" + areaLevel +
        ", areaOrder=" + areaOrder +
        ", areaNameEn=" + areaNameEn +
        ", areaShortnameEn=" + areaShortnameEn +
        "}";
    }
}
