package com.j4cf.lx.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.lx.dao.entity.LxUser;
import com.j4cf.lx.service.LxUserService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* @Description: Lxuser controller
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
@RestController
@RequestMapping("/LxUser")
@Api(value = "Lxuser控制器", description = "Lxuser管理")
public class LxUserController extends BaseApiController<LxUser,LxUserService> implements BaseApiService<LxUser>{

    private static final Logger LOGGER = LoggerFactory.getLogger(LxUserController.class);

    @Override
    @ApiOperation(value = "新增-Lxuser-信息",notes = "新增-Lxuser-信息")
    @RequiresPermissions("lx:lxuser:add")
    public BaseResult insert(@RequestBody LxUser entity) {
    return super.insert(entity);
    }
    
    @Override
    @ApiOperation(value = "根据ID删除-Lxuser-信息",notes = "根据ID删除-Lxuser-信息")
    @RequiresPermissions("lx:lxuser:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
    return super.deleteById(id);
    }
    
    @Override
    @ApiOperation(value = "根据ID修改-Lxuser-信息",notes = "根据ID修改-Lxuser-信息")
    @RequiresPermissions("lx:lxuser:update")
    public BaseResult updateById(@RequestBody LxUser entity) {
    return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询-Lxuser-信息",notes = "根据ID查询-Lxuser-信息")
    @RequiresPermissions("lx:lxuser:read")
    public BaseResult<LxUser> selectById(@RequestParam("id") String id) {
    return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有-Lxuser-信息",notes = "查询所有-Lxuser-信息")
    @RequiresPermissions("lx:lxuser:read")
    public BaseResult<List<LxUser>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params); 
    }
        
    @Override
    @ApiOperation(value = "查询-Lxuser-信息分页",notes = "查询-Lxuser-信息分页")
    @RequiresPermissions("lx:lxuser:read")
    public BaseResult<Page<LxUser>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

}