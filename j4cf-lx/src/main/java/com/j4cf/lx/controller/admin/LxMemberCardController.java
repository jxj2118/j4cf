package com.j4cf.lx.controller.admin;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.lx.dao.entity.LxMemberCard;
import com.j4cf.lx.service.LxMemberCardService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* @Description: Lxmembercard controller
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
@RestController
@RequestMapping("/admin/LxMemberCard")
@Api(value = "Lxmembercard控制器", description = "Lxmembercard管理")
public class LxMemberCardController extends BaseApiController<LxMemberCard,LxMemberCardService> implements BaseApiService<LxMemberCard>{

    private static final Logger LOGGER = LoggerFactory.getLogger(LxMemberCardController.class);

    @Override
    @ApiOperation(value = "新增-Lxmembercard-信息",notes = "新增-Lxmembercard-信息")
    @RequiresPermissions("lx:lxmembercard:add")
    public BaseResult insert(@RequestBody LxMemberCard entity) {
    return super.insert(entity);
    }
    
    @Override
    @ApiOperation(value = "根据ID删除-Lxmembercard-信息",notes = "根据ID删除-Lxmembercard-信息")
    @RequiresPermissions("lx:lxmembercard:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
    return super.deleteById(id);
    }
    
    @Override
    @ApiOperation(value = "根据ID修改-Lxmembercard-信息",notes = "根据ID修改-Lxmembercard-信息")
    @RequiresPermissions("lx:lxmembercard:update")
    public BaseResult updateById(@RequestBody LxMemberCard entity) {
    return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询-Lxmembercard-信息",notes = "根据ID查询-Lxmembercard-信息")
    @RequiresPermissions("lx:lxmembercard:read")
    public BaseResult<LxMemberCard> selectById(@RequestParam("id") String id) {
    return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有-Lxmembercard-信息",notes = "查询所有-Lxmembercard-信息")
    @RequiresPermissions("lx:lxmembercard:read")
    public BaseResult<List<LxMemberCard>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params); 
    }
        
    @Override
    @ApiOperation(value = "查询-Lxmembercard-信息分页",notes = "查询-Lxmembercard-信息分页")
    @RequiresPermissions("lx:lxmembercard:read")
    public BaseResult<Page<LxMemberCard>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

}