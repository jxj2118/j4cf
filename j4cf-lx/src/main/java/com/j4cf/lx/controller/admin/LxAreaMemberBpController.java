package com.j4cf.lx.controller.admin;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.lx.dao.entity.LxAreaMemberBp;
import com.j4cf.lx.service.LxAreaMemberBpService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* @Description: Lxareamemberbp controller
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
@RestController
@RequestMapping("/admin/LxAreaMemberBp")
@Api(value = "Lxareamemberbp控制器", description = "Lxareamemberbp管理")
public class LxAreaMemberBpController extends BaseApiController<LxAreaMemberBp,LxAreaMemberBpService> implements BaseApiService<LxAreaMemberBp>{

    private static final Logger LOGGER = LoggerFactory.getLogger(LxAreaMemberBpController.class);

    @Override
    @ApiOperation(value = "新增-Lxareamemberbp-信息",notes = "新增-Lxareamemberbp-信息")
    @RequiresPermissions("lx:lxareamemberbp:add")
    public BaseResult insert(@RequestBody LxAreaMemberBp entity) {
        //return super.insert(entity);
        return new BaseResult(BaseResultEnum.ERROR,"禁止手动添加");
    }
    
    @Override
    @ApiOperation(value = "根据ID删除-Lxareamemberbp-信息",notes = "根据ID删除-Lxareamemberbp-信息")
    @RequiresPermissions("lx:lxareamemberbp:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
        //return super.deleteById(id);
        return new BaseResult(BaseResultEnum.ERROR,"禁止手动删除");
    }
    
    @Override
    @ApiOperation(value = "根据ID修改-Lxareamemberbp-信息",notes = "根据ID修改-Lxareamemberbp-信息")
    @RequiresPermissions("lx:lxareamemberbp:update")
    public BaseResult updateById(@RequestBody LxAreaMemberBp entity) {
        entity.setMemberName(null);
        entity.setMemberTypeId(null);
        entity.setAreaName(null);
        entity.setAreaId(null);
        return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询-Lxareamemberbp-信息",notes = "根据ID查询-Lxareamemberbp-信息")
    @RequiresPermissions("lx:lxareamemberbp:read")
    public BaseResult<LxAreaMemberBp> selectById(@RequestParam("id") String id) {
    return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有-Lxareamemberbp-信息",notes = "查询所有-Lxareamemberbp-信息")
    @RequiresPermissions("lx:lxareamemberbp:read")
    public BaseResult<List<LxAreaMemberBp>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params); 
    }
        
    @Override
    @ApiOperation(value = "查询-Lxareamemberbp-信息分页",notes = "查询-Lxareamemberbp-信息分页")
    @RequiresPermissions("lx:lxareamemberbp:read")
    public BaseResult<Page<LxAreaMemberBp>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

}