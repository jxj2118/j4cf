package com.j4cf.lx.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.lx.dao.entity.LxGoodsCats;
import com.j4cf.lx.service.LxGoodsCatsService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* @Description: Lxgoodscats controller
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
@RestController
@RequestMapping("/LxGoodsCats")
@Api(value = "Lxgoodscats控制器", description = "Lxgoodscats管理")
public class LxGoodsCatsController extends BaseApiController<LxGoodsCats,LxGoodsCatsService> implements BaseApiService<LxGoodsCats>{

    private static final Logger LOGGER = LoggerFactory.getLogger(LxGoodsCatsController.class);

    @Override
    @ApiOperation(value = "新增-Lxgoodscats-信息",notes = "新增-Lxgoodscats-信息")
    @RequiresPermissions("lx:lxgoodscats:add")
    public BaseResult insert(@RequestBody LxGoodsCats entity) {
    return super.insert(entity);
    }
    
    @Override
    @ApiOperation(value = "根据ID删除-Lxgoodscats-信息",notes = "根据ID删除-Lxgoodscats-信息")
    @RequiresPermissions("lx:lxgoodscats:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
    return super.deleteById(id);
    }
    
    @Override
    @ApiOperation(value = "根据ID修改-Lxgoodscats-信息",notes = "根据ID修改-Lxgoodscats-信息")
    @RequiresPermissions("lx:lxgoodscats:update")
    public BaseResult updateById(@RequestBody LxGoodsCats entity) {
    return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询-Lxgoodscats-信息",notes = "根据ID查询-Lxgoodscats-信息")
    @RequiresPermissions("lx:lxgoodscats:read")
    public BaseResult<LxGoodsCats> selectById(@RequestParam("id") String id) {
    return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有-Lxgoodscats-信息",notes = "查询所有-Lxgoodscats-信息")
    @RequiresPermissions("lx:lxgoodscats:read")
    public BaseResult<List<LxGoodsCats>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params); 
    }
        
    @Override
    @ApiOperation(value = "查询-Lxgoodscats-信息分页",notes = "查询-Lxgoodscats-信息分页")
    @RequiresPermissions("lx:lxgoodscats:read")
    public BaseResult<Page<LxGoodsCats>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

}