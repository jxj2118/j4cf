package com.j4cf.lx.controller.admin;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.lx.dao.entity.LxCats;
import com.j4cf.lx.service.LxCatsService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* @Description: Lxcats controller
* @Author: LongRou
* @CreateDate: 2018/5/21.
* @Version: 1.0
**/
@RestController
@RequestMapping("/admin/LxCats")
@Api(value = "Lxcats控制器", description = "Lxcats管理")
public class LxCatsController extends BaseApiController<LxCats,LxCatsService> implements BaseApiService<LxCats>{

    private static final Logger LOGGER = LoggerFactory.getLogger(LxCatsController.class);

    @Override
    @ApiOperation(value = "新增-Lxcats-信息",notes = "新增-Lxcats-信息")
    @RequiresPermissions("lx:lxcats:add")
    public BaseResult insert(@RequestBody LxCats entity) {
        entity.setCtime(System.currentTimeMillis());
        return super.insert(entity);
    }
    
    @Override
    @ApiOperation(value = "根据ID删除-Lxcats-信息",notes = "根据ID删除-Lxcats-信息")
    @RequiresPermissions("lx:lxcats:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
        try {
            try {
                int idx = Integer.parseInt(id);
                if (service.deleteAndChiById(idx)){return new BaseResult(BaseResultEnum.SUCCESS,"数据删除成功");}
                else {return new BaseResult(BaseResultEnum.ERROR,"删除失败");}
            }catch (NumberFormatException e){
                if (service.deleteAndChiById(id)){return new BaseResult(BaseResultEnum.SUCCESS,"数据删除成功");}
                else {return new BaseResult(BaseResultEnum.ERROR,"删除失败");}
            }
        }catch (Exception e){
            return new BaseResult(BaseResultEnum.ERROR,"删除失败,已使用的数据无法删除！");
        }
    }
    
    @Override
    @ApiOperation(value = "根据ID修改-Lxcats-信息",notes = "根据ID修改-Lxcats-信息")
    @RequiresPermissions("lx:lxcats:update")
    public BaseResult updateById(@RequestBody LxCats entity) {
    return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询-Lxcats-信息",notes = "根据ID查询-Lxcats-信息")
    @RequiresPermissions("lx:lxcats:read")
    public BaseResult<LxCats> selectById(@RequestParam("id") String id) {
    return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有-Lxcats-信息",notes = "查询所有-Lxcats-信息")
    //@RequiresPermissions("lx:lxcats:read")
    @RequiresRoles(value = {"agent","super","member"},logical = Logical.OR)
    public BaseResult<List<LxCats>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params); 
    }
        
    @Override
    @ApiOperation(value = "查询-Lxcats-信息分页",notes = "查询-Lxcats-信息分页")
    @RequiresPermissions("lx:lxcats:read")
    public BaseResult<Page<LxCats>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

}