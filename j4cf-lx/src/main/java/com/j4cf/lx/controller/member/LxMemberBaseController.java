package com.j4cf.lx.controller.member;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.auth.service.AuthApiService;
import com.j4cf.common.base.BaseController;
import com.j4cf.common.base.BaseQuery;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.lx.controller.agent.LxMemberManageController;
import com.j4cf.lx.dao.entity.LxBpRecord;
import com.j4cf.lx.dao.entity.LxMemberCard;
import com.j4cf.lx.dao.entity.LxNotice;
import com.j4cf.lx.dao.entity.LxUser;
import com.j4cf.lx.service.LxBpRecordService;
import com.j4cf.lx.service.LxNoticeService;
import com.j4cf.lx.service.LxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018-05-28 14:45
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/member/LxMemberBase")
@Api(value = "LLxMemberBase控制器", description = "LxMemberBase管理")
public class LxMemberBaseController extends BaseController{
    private static final Logger LOGGER = LoggerFactory.getLogger(LxMemberBaseController.class);
    @Autowired
    private AuthApiService authApiService;
    @Autowired
    private LxBpRecordService lxBpRecordService;
    @Autowired
    private LxNoticeService lxNoticeService;
    @Autowired
    private LxUserService lxUserService;

    @ApiOperation(value = "查询-Lx个人信息", notes = "查询-Lx个人信息")
    @RequiresRoles(value = {"member"})
    @RequestMapping(value = "/selectLxUser", method = RequestMethod.GET)
    public BaseResult<LxUser> selectLxUser() {
        return new BaseResult(BaseResultEnum.SUCCESS, lxUserService.selectById(getUserId()));
    }

    @ApiOperation(value = "查询-LxBpRecord-信息分页", notes = "查询-LxBpRecord-信息分页")
    @RequiresRoles(value = {"member"})
    @RequestMapping(value = "/selectRecordPage", method = RequestMethod.GET)
    public BaseResult<Page<LxBpRecord>> selectRecordPage(@RequestParam Map<String, Object> params) {
        params.put("user_id", getUserId());
        BaseQuery BaseQuery = new BaseQuery<LxBpRecord>(params);
        try {
            Page<LxBpRecord> tPage = new Page<>(BaseQuery.getPage(), BaseQuery.getSize());
            tPage.setSearchCount(true);
            return new BaseResult(BaseResultEnum.SUCCESS, lxBpRecordService.selectPage(tPage, BaseQuery.getEntityWrapper()));
        } catch (Exception e) {
            return new BaseResult(BaseResultEnum.ERROR, "请检查参数是否正确");
        }
    }

    @ApiOperation(value = "查询-LxNotice-信息分页", notes = "查询-LxNotice-信息分页")
    @RequiresRoles(value = {"member"})
    @RequestMapping(value = "/selectNoticePage", method = RequestMethod.GET)
    public BaseResult<Page<LxNotice>> selectNoticePage(@RequestParam Map<String, Object> params) {
        params.put("state", 0);
        BaseQuery BaseQuery = new BaseQuery<LxNotice>(params);
        try {
            Page<LxNotice> tPage = new Page<>(BaseQuery.getPage(), BaseQuery.getSize());
            tPage.setSearchCount(true);
            return new BaseResult(BaseResultEnum.SUCCESS, lxNoticeService.selectPage(tPage, BaseQuery.getEntityWrapper()));
        } catch (Exception e) {
            return new BaseResult(BaseResultEnum.ERROR, "请检查参数是否正确");
        }
    }



    protected Integer getUserId() {
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();
        AuthUser authUser = authApiService.selectAuthUserByUsernameByCache(username);
        if (null != authUser) {
            return authUser.getUserId();
        } else {
            return -1;
        }
    }
}
