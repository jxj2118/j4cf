package com.j4cf.lx.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.lx.dao.entity.LxArea;
import com.j4cf.lx.service.LxAreaService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* @Description: Lxarea controller
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
@RestController
@RequestMapping("/LxArea")
@Api(value = "Lxarea控制器", description = "Lxarea管理")
public class LxAreaController extends BaseApiController<LxArea,LxAreaService> implements BaseApiService<LxArea>{

    private static final Logger LOGGER = LoggerFactory.getLogger(LxAreaController.class);

    @Override
    @ApiOperation(value = "新增-Lxarea-信息",notes = "新增-Lxarea-信息")
    @RequiresPermissions("lx:lxarea:add")
    public BaseResult insert(@RequestBody LxArea entity) {
    return super.insert(entity);
    }
    
    @Override
    @ApiOperation(value = "根据ID删除-Lxarea-信息",notes = "根据ID删除-Lxarea-信息")
    @RequiresPermissions("lx:lxarea:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
    return super.deleteById(id);
    }
    
    @Override
    @ApiOperation(value = "根据ID修改-Lxarea-信息",notes = "根据ID修改-Lxarea-信息")
    @RequiresPermissions("lx:lxarea:update")
    public BaseResult updateById(@RequestBody LxArea entity) {
    return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询-Lxarea-信息",notes = "根据ID查询-Lxarea-信息")
    @RequiresPermissions("lx:lxarea:read")
    public BaseResult<LxArea> selectById(@RequestParam("id") String id) {
    return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有-Lxarea-信息",notes = "查询所有-Lxarea-信息")
    @RequiresPermissions("lx:lxarea:read")
    public BaseResult<List<LxArea>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params); 
    }
        
    @Override
    @ApiOperation(value = "查询-Lxarea-信息分页",notes = "查询-Lxarea-信息分页")
    @RequiresPermissions("lx:lxarea:read")
    public BaseResult<Page<LxArea>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

}