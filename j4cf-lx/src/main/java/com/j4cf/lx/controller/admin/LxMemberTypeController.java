package com.j4cf.lx.controller.admin;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.common.validator.FixedLengthValidator;
import com.j4cf.common.validator.LengthValidator;
import com.j4cf.common.validator.NotNullValidator;
import com.j4cf.lx.dao.entity.LxMemberType;
import com.j4cf.lx.service.LxMemberTypeService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Description: Lxmembertype controller
 * @Author: LongRou
 * @CreateDate: 2018/5/8.
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/admin/LxMemberType")
@Api(value = "Lxmembertype控制器", description = "Lxmembertype管理")
public class LxMemberTypeController extends BaseApiController<LxMemberType, LxMemberTypeService> implements BaseApiService<LxMemberType> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxMemberTypeController.class);

    @Override
    @ApiOperation(value = "新增会员类型信息", notes = "新增会员类型信息")
    @RequiresPermissions("lx:lxmembertype:add")
    public BaseResult insert(@RequestBody LxMemberType entity) {
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(entity.getName(), new LengthValidator(2, 20, "会员名称"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        entity.setCtime(System.currentTimeMillis());
        if (service.insertAutoGenAMB(entity)){
            return new BaseResult(BaseResultEnum.SUCCESS,"添加成功！");
        }else {
            return new BaseResult(BaseResultEnum.ERROR,"添加失败！");
        }
    }

    @Override
    @ApiOperation(value = "根据ID删除会员类型信息", notes = "根据ID删除会员类型信息")
    @RequiresPermissions("lx:lxmembertype:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
        return super.deleteById(id);
    }

    @Override
    @ApiOperation(value = "根据ID修改会员类型信息", notes = "根据ID修改会员类型信息")
    @RequiresPermissions("lx:lxmembertype:update")
    public BaseResult updateById(@RequestBody LxMemberType entity) {
        return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询会员类型信息", notes = "根据ID查询会员类型信息")
    @RequiresPermissions("lx:lxmembertype:read")
    public BaseResult<LxMemberType> selectById(@RequestParam("id") String id) {
        return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有会员类型信息", notes = "查询所有会员类型信息")
    @RequiresPermissions("lx:lxmembertype:read")
    public BaseResult<List<LxMemberType>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params);
    }

    @Override
    @ApiOperation(value = "查询会员类型信息分页", notes = "查询会员类型信息分页")
    @RequiresPermissions("lx:lxmembertype:read")
    public BaseResult<Page<LxMemberType>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }
}