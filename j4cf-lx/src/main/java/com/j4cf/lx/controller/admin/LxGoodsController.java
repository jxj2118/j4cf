package com.j4cf.lx.controller.admin;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.*;
import com.j4cf.common.validator.NotNullValidator;
import com.j4cf.lx.dao.entity.LxGoods;
import com.j4cf.lx.dao.entity.LxGoodsCats;
import com.j4cf.lx.service.LxGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Description: Lxgoods controller
 * @Author: LongRou
 * @CreateDate: 2018/5/21.
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/admin/LxGoods")
@Api(value = "Lxgoods控制器", description = "Lxgoods管理")
public class LxGoodsController extends BaseApiController<LxGoods, LxGoodsService> implements BaseApiService<LxGoods> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LxGoodsController.class);

    @Override
//    @RequiresPermissions("lx:lxgoods:add")
//    public BaseResult insert(LxGoods entity) {
//        return new BaseResult(BaseResultEnum.ERROR, "被禁止的权限");
//    }

    @ApiOperation(value = "新增-Lxgoods-信息", notes = "新增-Lxgoods-信息")
    @RequiresPermissions("lx:lxgoods:add")
    @RequestMapping(value = "/insertAndCats", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public BaseResult insert(@RequestBody LxGoods entity) {
        entity.setCtime(System.currentTimeMillis());
        entity.setKeywords(entity.getGoodsName()+","+entity.getKeywords());
        if (service.insertAndCats(entity,entity.getCatId())){
            return new BaseResult(BaseResultEnum.SUCCESS,"添加成功！");
        }else {
            return new BaseResult(BaseResultEnum.ERROR,"添加失败！");
        }
    }

    @Override
    @ApiOperation(value = "根据ID删除-Lxgoods-信息", notes = "根据ID删除-Lxgoods-信息")
    @RequiresPermissions("lx:lxgoods:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
        return super.deleteById(id);
    }

    @Override
    @ApiOperation(value = "根据ID修改-Lxgoods-信息", notes = "根据ID修改-Lxgoods-信息")
    @RequiresPermissions("lx:lxgoods:update")
    public BaseResult updateById(@RequestBody LxGoods entity) {
        return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询-Lxgoods-信息", notes = "根据ID查询-Lxgoods-信息")
    //@RequiresPermissions("lx:lxgoods:read")
    @RequiresRoles(value = {"agent","super","member"},logical = Logical.OR)
    public BaseResult<LxGoods> selectById(@RequestParam("id") String id) {
        return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有-Lxgoods-信息", notes = "查询所有-Lxgoods-信息")
    @RequiresPermissions("lx:lxgoods:read")
    public BaseResult<List<LxGoods>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params);
    }

    @Override
    @ApiOperation(value = "查询-Lxgoods-信息分页", notes = "查询-Lxgoods-信息分页")
    //@RequiresPermissions("lx:lxgoods:read")
    @RequiresRoles(value = {"agent","super","member"},logical = Logical.OR)
    public BaseResult<Page<LxGoods>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

    @ApiOperation(value = "查询-Lxgoods-信息分页-根据分类Id", notes = "查询-Lxgoods-信息分页-根据分类Id")
    @RequiresRoles(value = {"agent","super","member"},logical = Logical.OR)
    @RequestMapping(value = "/selectPageByCat" ,method = RequestMethod.GET)
    public BaseResult<Page<LxGoods>> selectPageByCat(@RequestParam Map<String, Object> params) {
        //信息校验
        ComplexResult result = FluentValidator.checkAll()
                .on(params.get("cat_id") + "", new NotNullValidator("分类信息"))
                .doValidate()
                .result(ResultCollectors.toComplex());
        if (!result.isSuccess()) {
            return new BaseResult(BaseResultEnum.ERROR, result.getErrors().get(0).getErrorMsg());
        }
        BaseQuery BaseQuery = new BaseQuery<LxGoodsCats>(params);
        try {
            Page<LxGoodsCats> tPage =new Page<>(BaseQuery.getPage(),BaseQuery.getSize());
            tPage.setSearchCount(true);
            return new BaseResult(BaseResultEnum.SUCCESS,service.selectPageByCatId(tPage,BaseQuery.getEntityWrapper()));
        }catch (Exception e){
            return new BaseResult(BaseResultEnum.ERROR,"请检查参数是否正确");
        }
    }

}