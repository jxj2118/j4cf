package com.j4cf.lx.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.lx.dao.entity.LxBpRecord;
import com.j4cf.lx.service.LxBpRecordService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* @Description: Lxbprecord controller
* @Author: LongRou
* @CreateDate: 2018/5/8.
* @Version: 1.0
**/
@RestController
@RequestMapping("/LxBpRecord")
@Api(value = "Lxbprecord控制器", description = "Lxbprecord管理")
public class LxBpRecordController extends BaseApiController<LxBpRecord,LxBpRecordService> implements BaseApiService<LxBpRecord>{

    private static final Logger LOGGER = LoggerFactory.getLogger(LxBpRecordController.class);

    @Override
    @ApiOperation(value = "新增-Lxbprecord-信息",notes = "新增-Lxbprecord-信息")
    @RequiresPermissions("lx:lxbprecord:add")
    public BaseResult insert(@RequestBody LxBpRecord entity) {
    return super.insert(entity);
    }
    
    @Override
    @ApiOperation(value = "根据ID删除-Lxbprecord-信息",notes = "根据ID删除-Lxbprecord-信息")
    @RequiresPermissions("lx:lxbprecord:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
    return super.deleteById(id);
    }
    
    @Override
    @ApiOperation(value = "根据ID修改-Lxbprecord-信息",notes = "根据ID修改-Lxbprecord-信息")
    @RequiresPermissions("lx:lxbprecord:update")
    public BaseResult updateById(@RequestBody LxBpRecord entity) {
    return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询-Lxbprecord-信息",notes = "根据ID查询-Lxbprecord-信息")
    @RequiresPermissions("lx:lxbprecord:read")
    public BaseResult<LxBpRecord> selectById(@RequestParam("id") String id) {
    return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有-Lxbprecord-信息",notes = "查询所有-Lxbprecord-信息")
    @RequiresPermissions("lx:lxbprecord:read")
    public BaseResult<List<LxBpRecord>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params); 
    }
        
    @Override
    @ApiOperation(value = "查询-Lxbprecord-信息分页",notes = "查询-Lxbprecord-信息分页")
    @RequiresPermissions("lx:lxbprecord:read")
    public BaseResult<Page<LxBpRecord>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

}