package com.j4cf.lx.controller.agent;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.auth.dao.entity.AuthUser;
import com.j4cf.auth.service.AuthApiService;
import com.j4cf.common.base.BaseController;
import com.j4cf.common.base.BaseQuery;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.lx.controller.member.LxMemberBaseController;
import com.j4cf.lx.dao.entity.LxNotice;
import com.j4cf.lx.service.LxBpRecordService;
import com.j4cf.lx.service.LxNoticeService;
import com.j4cf.lx.service.LxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018-05-28 14:51
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/agent/LxAgentBase")
@Api(value = " LxAgentBase控制器", description = " LxAgentBase管理")
public class LxAgentBaseController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LxMemberBaseController.class);
    @Autowired
    private AuthApiService authApiService;
    @Autowired
    private LxNoticeService lxNoticeService;


    @ApiOperation(value = "查询-LxNotice-信息分页", notes = "查询-LxNotice-信息分页")
    @RequiresRoles(value = {"agent","member"},logical = Logical.OR)
    @RequestMapping(value = "/selectNoticePage", method = RequestMethod.GET)
    public BaseResult<Page<LxNotice>> selectNoticePage(@RequestParam Map<String, Object> params) {
        params.put("state", 0);
        BaseQuery BaseQuery = new BaseQuery<LxNotice>(params);
        try {
            Page<LxNotice> tPage = new Page<>(BaseQuery.getPage(), BaseQuery.getSize());
            tPage.setSearchCount(true);
            return new BaseResult(BaseResultEnum.SUCCESS, lxNoticeService.selectPage(tPage, BaseQuery.getEntityWrapper()));
        } catch (Exception e) {
            return new BaseResult(BaseResultEnum.ERROR, "请检查参数是否正确");
        }
    }
    private Integer getUserId() {
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();
        AuthUser authUser = authApiService.selectAuthUserByUsernameByCache(username);
        if (null != authUser) {
            return authUser.getUserId();
        } else {
            return -1;
        }
    }
}
