package com.j4cf.lx.controller.admin;

import com.baomidou.mybatisplus.plugins.Page;
import com.j4cf.common.base.BaseResult;
import com.j4cf.lx.dao.entity.LxNotice;
import com.j4cf.lx.service.LxNoticeService;
import com.j4cf.common.base.BaseApiService;
import com.j4cf.common.base.BaseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* @Description: Lxnotice controller
* @Author: LongRou
* @CreateDate: 2018/5/13.
* @Version: 1.0
**/
@RestController
@RequestMapping("/admin/LxNotice")
@Api(value = "Lxnotice控制器", description = "Lxnotice管理")
public class LxNoticeController extends BaseApiController<LxNotice,LxNoticeService> implements BaseApiService<LxNotice>{

    private static final Logger LOGGER = LoggerFactory.getLogger(LxNoticeController.class);

    @Override
    @ApiOperation(value = "新增LX系统公告信息",notes = "新增LX系统公告信息")
    @RequiresPermissions("lx:lxnotice:add")
    public BaseResult insert(@RequestBody LxNotice entity) {
        entity.setCtime(System.currentTimeMillis());
        return super.insert(entity);
    }
    
    @Override
    @ApiOperation(value = "根据ID删除LX系统公告信息",notes = "根据ID删除LX系统公告信息")
    @RequiresPermissions("lx:lxnotice:delete")
    public BaseResult deleteById(@RequestParam("id") String id) {
    return super.deleteById(id);
    }
    
    @Override
    @ApiOperation(value = "根据ID修改LX系统公告信息",notes = "根据ID修改LX系统公告信息")
    @RequiresPermissions("lx:lxnotice:update")
    public BaseResult updateById(@RequestBody LxNotice entity) {
    return super.updateById(entity);
    }

    @Override
    @ApiOperation(value = "根据ID查询LX系统公告信息",notes = "根据ID查询LX系统公告信息")
    @RequiresPermissions("lx:lxnotice:read")
    public BaseResult<LxNotice> selectById(@RequestParam("id") String id) {
    return super.selectById(id);
    }

    @Override
    @ApiOperation(value = "查询所有LX系统公告信息",notes = "查询所有LX系统公告信息")
    @RequiresPermissions("lx:lxnotice:read")
    public BaseResult<List<LxNotice>> selectAll(@RequestParam Map<String, Object> params) {
        return super.selectAll(params); 
    }
        
    @Override
    @ApiOperation(value = "查询LX系统公告信息分页",notes = "查询LX系统公告信息分页")
    @RequiresPermissions("lx:lxnotice:read")
    public BaseResult<Page<LxNotice>> selectPage(@RequestParam Map<String, Object> params) {
        return super.selectPage(params);
    }

}