package com.j4cf.lx.controller.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.j4cf.common.base.BaseController;
import com.j4cf.common.base.BaseResult;
import com.j4cf.common.base.BaseResultEnum;
import com.j4cf.common.util.TimeUtil;
import com.j4cf.lx.dao.entity.LxUser;
import com.j4cf.lx.dao.mapper.LxAdminMapper;
import com.j4cf.lx.service.LxMemberCardService;
import com.j4cf.lx.service.LxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.session.SqlSession;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018-05-28 14:51
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/admin/LxAdminBase")
@Api(value = " LxAdminBase控制器", description = " LxAdminBase管理")
public class LxAdminBaseController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LxAdminBaseController.class);
    @Autowired
    private LxUserService lxUserService;
    @Autowired
    private LxMemberCardService lxMemberCardService;
    @Autowired
    private SqlSession sqlSession;


    @ApiOperation(value = "查询-聚合-信息分页", notes = "查询-聚合-信息分页")
    @RequiresRoles(value = {"super"})
    @RequestMapping(value = "/selectLxSystemInfo", method = RequestMethod.GET)
    public BaseResult selectLxSystemInfo() throws Exception{
        Map<String,Object> resultMap = new HashMap<>();
        int totalMember = lxUserService.selectCount(new EntityWrapper<>());
        int totalDayMember = lxUserService.selectCount(new EntityWrapper<LxUser>().ge("ctime", TimeUtil.getCurrentDateLong()));
        LxAdminMapper lxAdminService = sqlSession.getMapper(LxAdminMapper.class);
        int totalConsume = lxAdminService.getTotalConsumePrice();
        int totalDayConsume = lxAdminService.getTotalDayConsumePrice(TimeUtil.getCurrentDateLong());

        resultMap.put("totalMember",totalMember);
        resultMap.put("totalDayMember",totalDayMember);
        resultMap.put("totalConsume",totalConsume);
        resultMap.put("totalDayConsume",totalDayConsume);
        return new BaseResult(BaseResultEnum.SUCCESS, resultMap);
    }
}
