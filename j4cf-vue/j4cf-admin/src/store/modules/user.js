import Cookies from 'js-cookie';
import {
  login,
  logout,
  getInfo,
  getMenus,
  getRole
} from 'api/login';
import {
  getUserInfo
} from 'api/user/user';
import {
  getToken,
  setToken,
  removeToken
} from 'utils/auth';

const user = {
    state: {
      //menus:undefined,
      token: getToken()
    },
    actions: {
      // 登录
      Login({
        commit
      }, userInfo) {
        console.info("actions = > login")
        const username = userInfo.username.trim();
        return new Promise((resolve, reject) => {
          login(username, userInfo.password).then(response => {
        //getUserInfo().then(response => {
            const data = response;
            if (data.status === 200) {
              Cookies.set('token', data.data.token);
              commit('SET_TOKEN', data.data.token);
              resolve();
            } else {
              reject(data);
            }
          }).catch(error => {
            reject(error);
          });
        });
      },
      GetUserRole({commit}){
        console.info("actions = > GetUserRole")
        return new Promise((resolve, reject) => {
          getRole().then(response => {
            const data = response;
            if (data.status === 200) {
              if (data.data[0].name === 'super') {
                Cookies.set('user', data.data);
                Cookies.set('locked', data.data.locked);
                resolve();
              }else {
                reject(new Error('请检查用户是否为管理员！'));
                Cookies.remove('token');
                commit('SET_TOKEN', undefined);
                logout().then(response => {});
              }
            } else {
              reject(data);
            }
          }).catch(error => {
            error.message = '网络错误';
            reject(error);
          });
        });
      },
      GetUserInfo({commit}){
        return new Promise((resolve, reject) => {
          getUserInfo().then(response => {
            console.info("actions = > GetUserInfo111")
            const data = response;
            if (data.status === 200) {
              Cookies.set('user', data.data);
              Cookies.set('locked', data.data.locked);
              resolve();
            } else {
              reject(data);
            }
          }).catch(error => {
            error.message = '网络错误';
            reject(error);
          });
        });
      },
      GetMenus({
        commit
      }) {
        return new Promise((resolve, reject) => {
          getMenus().then(response => {
            const data = response;
            if (data.status === 200) {
              commit('SET_MENUS',data.data);
              //commit('SET_MENUS', data.data);
              resolve();
            } else {
              reject(data);
            }
          }).catch(error => {
            reject(error);
          });
        });
      },
        // 前端 登出
      FedLogOut({
        commit
      }) {
        return new Promise(resolve => {
          Cookies.remove('token');
          Cookies.remove('user');
          Cookies.remove('locked');
          Cookies.remove('menu');
          // 恢复默认样式
          let themeLink = document.querySelector('link[name="theme"]');
          themeLink.setAttribute('href', '');
          // 清空打开的页面等数据，但是保存主题数据
          let theme = '';
          if (localStorage.theme) {
              theme = localStorage.theme;
          }
          localStorage.clear();
          if (theme) {
              localStorage.theme = theme;
          }
          resolve();
        });
      }
    },
    mutations: {
      // SET_MENUS: (state, menus) => {
      //   state.menus = menus;
      // },
      SET_TOKEN: (state, token) => {
        state.token = token;
      },
        logout (state, vm) {
            Cookies.remove('token');
            Cookies.remove('user');
            Cookies.remove('locked');
            Cookies.remove('menu');
            // 恢复默认样式
            let themeLink = document.querySelector('link[name="theme"]');
            themeLink.setAttribute('href', '');
            // 清空打开的页面等数据，但是保存主题数据
            let theme = '';
            if (localStorage.theme) {
                theme = localStorage.theme;
            }
            localStorage.clear();
            if (theme) {
                localStorage.theme = theme;
            }
        }
    }
};

export default user;
