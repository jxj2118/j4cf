import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取系统分页
export function getPermissionPage(page,size) {
  const data = {
    page,
    size
  };
  return fetch({
    url: '/manage/AuthPermission/selectPage' + getParams(data),
    method: 'get'
  });
}
//根据ID获取权限基本信息
export function getPermissionById(id) {
  const data = {
    id
  };
  return fetch({
    url: '/manage/AuthPermission/deleteById' + getParams(data),
    method: 'get'
  });
}
//根据系统ID获取权限基本信息
export function getAllPermission() {
  return fetch({
    url: '/manage/AuthPermission/selectAll',
    method: 'get'
  });
}
//添加权限信息
export function addPermission(obj) {
  return fetch({
    url: '/manage/AuthPermission/insert',
    method: 'POST',
    data:obj
  });
}
//添加权限信息
export function addPermissionAuto(obj) {
  return fetch({
    url: '/manage/AuthPermission/add/PermissionAuto',
    method: 'POST',
    data:obj
  });
}

//修改权限信息
export function putPermission(obj) {
  return fetch({
    url: '/manage/AuthPermission/updateById',
    method: 'PUT',
    data:obj
  });
}
//删除权限信息
export function delPermission(id) {
  const data = {
    id:id
  };
  return fetch({
    url: '/permission/delete/PermissionAuto'+ getParams(data),
    method: 'delete'
  });
}
