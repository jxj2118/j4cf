import fetch from 'utils/fetch';
//获取用户信息
export function getUserInfo() {
  return fetch({
    url: '/user/admin/getUserInfo',
    method: 'get'
  });
}
//用户修改个人信息
export function updateUserInfo(obj) {
  return fetch({
    url: '/user/admin/updateUserInfo',
    method: 'PUT',
    data:obj
  });
}
//用户修改个人密码
export function updateUserPass(oldPass,newPass) {
  const data = {
    oldPass,
    newPass
  };
  return fetch({
    url: '/user/admin/updatePass',
    method: 'PUT',
    data:data
  });
}
