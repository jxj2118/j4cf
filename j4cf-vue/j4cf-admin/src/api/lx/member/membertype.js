import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取会员类型分页
export function getMemberTypePage(page,size) {
  const data = {
    page,
    size
  };
  return fetch({
    url: '/admin/LxMemberType/selectPage' + getParams(data),
    method: 'get'
  });
}
//获取所有会员类型
export function getMemberTypeAll() {
  return fetch({
    url: '/admin/LxMemberType/selectAll',
    method: 'get'
  });
}
//添加会员类型
export function addMemberType(obj) {
  return fetch({
    url: '/admin/LxMemberType/insert',
    method: 'POST',
    data:obj
  });
}
//修改会员类型
export function putMemberType(obj) {
  return fetch({
    url: '/admin/LxMemberType/updateById',
    method: 'PUT',
    data:obj
  });
}
//删除会员类型
export function delMemberType(id) {
  const data = {
    id:id
  };
  return fetch({
    url: '/admin/LxMemberType/deleteById'+ getParams(data),
    method: 'delete'
  });
}
