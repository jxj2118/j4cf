import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取代理商分页
export function getAgentPage(page,size) {
  const data = {
    page,
    size
  };
  return fetch({
    url: '/admin/LxAgent/selectPage' + getParams(data),
    method: 'get'
  });
}
//获取所有代理商
export function getAgentAll() {
  return fetch({
    url: '/admin/LxAgent/selectAll',
    method: 'get'
  });
}
//添加代理商
export function addAgent(obj) {
  return fetch({
    url: '/admin/LxAgent/insert',
    method: 'POST',
    data:obj
  });
}
//修改代理商
export function putAgent(obj) {
  return fetch({
    url: '/admin/LxAgent/updateById',
    method: 'PUT',
    data:obj
  });
}
//根据代理商账号重置代理商密码
export function resetAgent(obj) {
  return fetch({
    url: '/admin/LxAgent/resetAgentUserPassword',
    method: 'PUT',
    data:obj
  });
}

//获取代理商下会员卡分页
export function getAgentInfoPage(page,size,agentUserId) {
  const data = {
    page,
    size,
    agentUserId:agentUserId
  };
  return fetch({
    url: '/admin/LxAgent/selectLxAgentInfoPage' + getParams(data),
    method: 'get'
  });
}
//获取所有会员类型
export function getMemberTypeAll() {
  return fetch({
    url: '/admin/LxAgent/selectAllLxMemberType',
    method: 'get'
  });
}
//代理商下级会员额度添加
export function addMemberForAgent(obj) {
  // const data = {
  //   memberTypeId,cardNum,consumeMin,otime,isFree
  // };
  return fetch({
    url: '/admin/LxAgent/addLxMemberCardForAgent',
    method: 'POST',
    data:obj
  });
}
