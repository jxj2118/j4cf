import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取系统信息
export function getLxSystemInfo() {
  return fetch({
    url: '/admin/LxAdminBase/selectLxSystemInfo',
    method: 'get'
  });
}
