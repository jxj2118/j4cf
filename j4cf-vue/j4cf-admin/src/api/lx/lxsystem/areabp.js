import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取区域返利信息分页
export function getAreaMemberBpPage(page,size) {
  const data = {
    page,
    size
  };
  return fetch({
    url: '/admin/LxAreaMemberBp/selectPage' + getParams(data),
    method: 'get'
  });
}
// 获取所有区域返利信息
export function getAreaMemberBpAll(query) {
  const data = {
  }
  for (var k in query) {
    if (k === 'type' && query[k] === 0) {
      continue
    }
    data[k] = query[k]
  }
  return fetch({
    url: '/admin/LxAreaMemberBp/selectAll' + getParams(data),
    method: 'get'
  });
}
//修改区域返利信息
export function putAreaMemberBp(obj) {
  return fetch({
    url: '/admin/LxAreaMemberBp/updateById',
    method: 'PUT',
    data:obj
  });
}
