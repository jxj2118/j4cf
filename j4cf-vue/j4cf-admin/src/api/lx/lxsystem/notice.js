import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取系统公告分页
export function getNoticePage(page,size) {
  const data = {
    page,
    size
  };
  return fetch({
    url: '/admin/LxNotice/selectPage' + getParams(data),
    method: 'get'
  });
}
//获取所有系统公告
export function getNoticeAll() {
  return fetch({
    url: '/admin/LxNotice/selectAll',
    method: 'get'
  });
}
//添加系统公告
export function addNotice(obj) {
  return fetch({
    url: '/admin/LxNotice/insert',
    method: 'POST',
    data:obj
  });
}
//修改系统公告
export function putNotice(obj) {
  return fetch({
    url: '/admin/LxNotice/updateById',
    method: 'PUT',
    data:obj
  });
}
//删除系统公告
export function delNotice(id) {
  const data = {
    id:id
  };
  return fetch({
    url: '/admin/LxNotice/deleteById'+ getParams(data),
    method: 'delete'
  });
}
