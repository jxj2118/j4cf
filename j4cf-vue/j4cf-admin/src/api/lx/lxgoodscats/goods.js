import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取商品分页
export function getGoodsPage(page,size) {
  const data = {
    page,
    size
  };
  return fetch({
    url: '/admin/LxGoods/selectPage' + getParams(data),
    method: 'get'
  });
}
//获取所有商品
export function getGoodsAll() {
  return fetch({
    url: '/admin/LxGoods/selectAll',
    method: 'get'
  });
}
//添加商品
export function addGoods(obj) {
  return fetch({
    url: '/admin/LxGoods/insertAndCats',
    method: 'POST',
    data:obj
  });
}
//修改商品
export function putGoods(obj) {
  return fetch({
    url: '/admin/LxGoods/updateById',
    method: 'PUT',
    data:obj
  });
}
//删除商品
export function delGoods(id) {
  const data = {
    id:id
  };
  return fetch({
    url: '/admin/LxGoods/deleteById'+ getParams(data),
    method: 'delete'
  });
}
