import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取商品分类分页
export function getCatsPage(page,size) {
  const data = {
    page,
    size
  };
  return fetch({
    url: '/admin/LxCats/selectPage' + getParams(data),
    method: 'get'
  });
}
//获取所有商品分类
export function getCatsAll() {
  return fetch({
    url: '/admin/LxCats/selectAll',
    method: 'get'
  });
}
//添加商品分类
export function addCats(obj) {
  return fetch({
    url: '/admin/LxCats/insert',
    method: 'POST',
    data:obj
  });
}
//修改商品分类
export function putCats(obj) {
  return fetch({
    url: '/admin/LxCats/updateById',
    method: 'PUT',
    data:obj
  });
}
//删除商品分类
export function delCats(id) {
  const data = {
    id:id
  };
  return fetch({
    url: '/admin/LxCats/deleteById'+ getParams(data),
    method: 'delete'
  });
}
