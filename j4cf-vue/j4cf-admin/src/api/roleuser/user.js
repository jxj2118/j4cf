import fetch from 'utils/fetch';
import {
  getParams
} from 'utils/params';
//获取角色分页
export function getUserPage(page,size) {
  const data = {
    page,
    size
  };
  return fetch({
    url: '/manage/AuthUser/selectPage' + getParams(data),
    method: 'get'
  });
}
//根据ID获取角色基本信息
export function getUserById(id) {
  const data = {
    id
  };
  return fetch({
    url: '/manage/AuthUser/selectById' + getParams(data),
    method: 'get'
  });
}
//根据角色ID获取角色基本信息
// export function getUserBySystemId(id) {
//   const data = {
//     system_id:id
//   };
//   return fetch({
//     url: '/user/select/AllUser' + getParams(data),
//     method: 'get'
//   });
// }
//根据用户ID获取所有权限
export function getPermissionByUserId(id) {
  const data = {
    id:id
  };
  return fetch({
    url: '/manage/AuthUser/select/getPermissionByUserId' + getParams(data),
    method: 'get'
  });
}
export function getPermissionByUserIdWithOutRole(id) {
  const data = {
    id:id
  };
  return fetch({
    url: '/manage/AuthUser/select/getPermissionByUserIdWithOutRole' + getParams(data),
    method: 'get'
  });
}
//添加角色信息
export function addUser(obj) {
  return fetch({
    url: '/manage/AuthUser/insert',
    method: 'POST',
    data:obj
  });
}
//addPermissionByUserId
export function addRoleByUserId(id,obj) {
  const data = {
    userId:id,
    list:obj
  };
  return fetch({
    url: '/manage/AuthUser/add/addRoleByUserId',
    method: 'POST',
    data
  });
}
//addPermissionByUserId
export function addPermissionByUserId(id,obj) {
  const data = {
    rodeId:id,
    list:obj
  };
  return fetch({
    url: '/manage/AuthUser/add/addPermissionByUserId',
    method: 'POST',
    data
  });
}
