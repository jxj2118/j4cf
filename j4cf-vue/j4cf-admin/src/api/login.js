import fetch from 'utils/fetch';

export function login(username, password) {
  const data = {
    username,
    password
  };
  return fetch({
    url: '/sso/login',
    method: 'post',
    data
  });
}

export function logout() {
  return fetch({
    url: '/sso/logout',
    method: 'get'
  });
}
//
// export function getInfo(token) {
//   return fetch({
//     url: '/api/admin/user/front/info',
//     method: 'get',
//     params: { token }
//   });
// }

export function getMenus() {
  return fetch({
    url: '/user/admin/getPermission',
    method: 'get'
  });
}
export function getRole() {
  return fetch({
    url: '/user/admin/getRole',
    method: 'get'
  });
}
