export const columns = [
    // {
    //     title: '权限ID',
    //     width: 80,
    //     key: 'permissionId',
    //     align: 'center'
    // },
    {
        title: '代理商账号',
        align: 'center',
        key: 'username'
    },
    {
        title: '真实姓名',
        align: 'center',
        key: 'realname',
        editable: true
    },
    {
        title: '电话',
        align: 'center',
        key: 'phone',
        editable: true
    },
    {
        title: '是否冻结',
        align: 'center',
        key: 'locked',
        render: (h, params) => {
                           return h('div', [
                               h('Tag', {
                                   props: {
                                       color: params.row.locked ? 'yellow' : 'green'
                                   }
                               },params.row.locked ? '已冻结' : '正常')
                           ]);
                       }
    },
    {
        title: '创建时间',
        align: 'center',
        key: 'ctime'
    },
    {
        title: '操作',
        align: 'center',
        width: 380,
        key: 'handle',
        handle: ['info','edit','delete','route']
    }
];

export default columns;
