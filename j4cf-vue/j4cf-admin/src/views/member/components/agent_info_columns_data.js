import {
  formatDateByLong
} from 'utils/time';
export const columns = [
    {
        title: '#',
        width: 80,
        key: 'memberCardId',
        align: 'center'
    },
    {
        title: '会员类型',
        align: 'center',
        key: 'name'
    },
    {
        title: '最大可获得返利积分',
        align: 'center',
        key: 'bpMax',
        editable: true
    },
    {
        title: '推荐人返佣积分',
        align: 'center',
        key: 'bpRecommend',
        editable: true
    },
    {
        title: '初始返利积分',
        align: 'center',
        key: 'bpInit',
        editable: true
    },
    {
        title: '创建时间',
        align: 'center',
        key: 'ctime',
        render: (h, params) => {
                           return h('div',formatDateByLong(params.row.ctime,"yyyy-MM-dd h:m"));
                       }
    },
    {
        title: '过期时间',
        align: 'center',
        key: 'otime',
        render: (h, params) => {
                           return h('div',formatDateByLong(params.row.otime,"yyyy-MM-dd"));
                       }
    },
    {
        title: '状态',
        align: 'center',
        key: 'state',
        render: (h, params) => {
                           return h('div', [
                               h('Tag', {
                                   props: {
                                       color: params.row.state == 0 ? 'blue':(params.row.state == 1? 'green' : 'red')
                                   }
                               },params.row.state == 0 ? '正常':(params.row.state == 1? '已绑定' : '过期'))
                           ]);
                       }
    },
    {
        title: '绑定用户',
        align: 'center',
        width: 180,
        key: 'username',
        render: (h, params) => {
                           return h('div',params.row.state == 1?(params.row.username+'('+params.row.realname+'-'+params.row.phone+')'):"");
                       }
    },
    {
        title: '操作',
        align: 'center',
        width: 260,
        key: 'handle',
        handle: ['info','delete']
        // ,'route'
    }
];

export default columns;
