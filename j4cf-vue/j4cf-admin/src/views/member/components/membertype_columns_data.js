export const columns = [
    {
        title: '类型ID',
        width: 80,
        key: 'memberTypeId',
        align: 'center'
    },
    {
        title: '会员类型名称',
        align: 'center',
        key: 'name',
        editable: true
    },
    {
        title: '最大可获得返利积分',
        align: 'center',
        key: 'bpMax'
    },
    {
        title: '推荐人返佣积分',
        align: 'center',
        key: 'bpRecommend'
    },
    {
        title: '初始返利积分',
        align: 'center',
        key: 'bpInit'
    },
    {
        title: '创建时间',
        align: 'center',
        key: 'ctime'
    },
    {
        title: '操作',
        align: 'center',
        width: 260,
        key: 'handle',
        handle: ['edit','delete']
    }
];

export default columns;
