import {
  formatDateByLong
} from 'utils/time';
export const columns = [
    {
        title: '#',
        width: 80,
        key: 'noticeId',
        align: 'center'
    },
    {
        title: '公告名称',
        width: 180,
        align: 'center',
        key: 'title',
        editable: true
    },
    {
        title: '公告内容',
        align: 'center',
        key: 'content',
        editable: true
    },
    {
        title: '创建时间',
        width: 160,
        align: 'center',
        key: 'ctime',
        render: (h, params) => {
                           return h('div',formatDateByLong(params.row.ctime,"yyyy-MM-dd h:m"));
                       }
    },
    {
        title: '排序',
        width: 80,
        align: 'center',
        key: 'order',
        editable: true
    },
    {
        title: '状态',
        width: 100,
        align: 'center',
        key: 'state',
        render: (h, params) => {
                           return h('div', [
                               h('Tag', {
                                   props: {
                                       color: params.row.state == 0 ? 'blue': 'red'
                                   }
                               },params.row.state == 0 ? '显示':'隐藏')
                           ]);
                       }
    },
    {
        title: '操作',
        align: 'center',
        width: 280,
        key: 'handle',
        handle: ['info','edit','delete']
    }
];

export default columns;
