import {
  formatDateByLong
} from 'utils/time';
export const columns = [
    {
        title: '#',
        width: 80,
        key: 'ambId',
        align: 'center'
    },
    {
        title: '会员类型',
        align: 'center',
        key: 'memberName'
    },
    {
        title: '区域名称',
        align: 'center',
        key: 'areaName'
    },
    {
        title: '每日返利积分',
        align: 'center',
        key: 'backBp',
        editable: true
    },
    {
        title: '操作',
        align: 'center',
        width: 160,
        key: 'handle',
        handle: ['edit']
    }
];

export default columns;
