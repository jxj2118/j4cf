export const columns = [
    {
        title: '序号',
        type: 'index',
        width: 80,
        key: 'buildId',
        align: 'center'
    },
    {
        title: '楼宇名称',
        align: 'center',
        key: 'name',
        editable: true
    },
    {
        title: '地址',
        align: 'center',
        key: 'address',
        editable: true
    },
    {
        title: '创建时间',
        align: 'center',
        key: 'ctime'
    },
    {
        title: '所属园区',
        align: 'center',
        key: 'park',
        editable: true
    },
    {
        title: '操作',
        align: 'center',
        width: 260,
        key: 'handle',
        handle: ['info','edit','delete']
    }
];

export default columns;
