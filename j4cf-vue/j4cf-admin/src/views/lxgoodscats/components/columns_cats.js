import {
  formatDateByLong
} from 'utils/time';
export const columns = [
    {
        title: '#',
        key: 'catId',
        align: 'center'
    }, {
        title: '分类名称',
        key: 'name',
        align: 'center',
        editable: true
    }, {
        title: '创建时间',
        key: 'ctiome',
        align: 'center',
        render: (h, params) => {
                           return h('div',formatDateByLong(params.row.ctime,"yyyy-MM-dd h:m"));
                       }
    },
    {
        title: '操作',
        align: 'center',
        width: 260,
        key: 'handle',
        handle: ['edit']
    }
];

export default columns;
