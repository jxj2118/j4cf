export const columns = [
    {
        title: '#',
        key: 'goodsId',
        align: 'center'
    }, {
        title: '角色名称',
        key: 'goodsName',
        align: 'center'
    }, {
        title: '角色缩略图',
        key: 'goodsThumb',
        align: 'center',
                  render: (h, params) => {
                                     return h('div', [
                                         h('img', {
                                            // 正常的 HTML 特性
                                            attrs: {
                                              src: params.row.goodsThumb
                                            },
                                              on: {
                                                  click: () => {
                                                      this.handleView(params.row.goodsName,params.row.goodsThumb)
                                                  }
                                              }
                                         })
                                     ]);
                                 }
    }, {
        title: '角色描述',
        key: 'description',
        align: 'center',
        editable: true
    }, {
        title: '创建时间',
        key: 'ctiome',
        align: 'center'
    },
    {
        title: '操作',
        align: 'center',
        width: 260,
        key: 'handle',
        handle: ['edit','delete']
    }
];

export default columns;
