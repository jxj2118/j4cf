export default [
  {
        areaId: 2,
        value:1,
        name: '北京市'
      },
      {
        areaId: 3,
        value:0,
        name: '天津市'
      },
      {
        areaId: 4,
        value:40,
        name: '河北省'
      },
      {
        areaId: 5,
        value:10,
        name: '山西省'
      },
      {
        areaId: 6,
        value:0,
        name: '内蒙古自治区'
      },
      {
        areaId: 7,
        value:0,
        name: '辽宁省'
      },
      {
        areaId: 8,
        value:0,
        name: '吉林省'
      },
      {
        areaId: 9,
        value:0,
        name: '黑龙江省'
      },
      {
        areaId: 10,
        value:0,
        name: '上海市'
      },
      {
        areaId: 11,
        value:0,
        name: '江苏省'
      },
      {
        areaId: 12,
        value:0,
        name: '浙江省'
      },
      {
        areaId: 13,
        value:0,
        name: '安徽省'
      },
      {
        areaId: 14,
        value:0,
        name: '福建省'
      },
      {
        areaId: 15,
        value:0,
        name: '江西省'
      },
      {
        areaId: 16,
        value:0,
        name: '山东省'
      },
      {
        areaId: 17,
        value:0,
        name: '河南省'
      },
      {
        areaId: 18,
        value:0,
        name: '湖北省'
      },
      {
        areaId: 19,
        value:0,
        name: '湖南省'
      },
      {
        areaId: 20,
        value:0,
        name: '广东省'
      },
      {
        areaId: 21,
        value:0,
        name: '广西壮族自治区'
      },
      {
        areaId: 22,
        value:0,
        name: '海南省'
      },
      {
        areaId: 23,
        value:0,
        name: '重庆市'
      },
      {
        areaId: 24,
        value:0,
        name: '四川省'
      },
      {
        areaId: 25,
        value:0,
        name: '贵州省'
      },
      {
        areaId: 26,
        value:0,
        name: '云南省'
      },
      {
        areaId: 27,
        value:0,
        name: '西藏自治区'
      },
      {
        areaId: 28,
        value:0,
        name: '陕西省'
      },
      {
        areaId: 29,
        value:0,
        name: '甘肃省'
      },
      {
        areaId: 30,
        value:0,
        name: '青海省'
      },
      {
        areaId: 31,
        value:0,
        name: '宁夏回族自治区'
      },
      {
        areaId: 32,
        value:0,
        name: '新疆维吾尔自治区'
      }
];
