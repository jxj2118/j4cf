package com.j4cf.common.base;

import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.session.InvalidSessionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/4/26 16:45
 * @Version: 1.0
 **/
public abstract class BaseController {
    private final static Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    /**
     * 统一异常处理
     * @param request
     * @param response
     * @param exception
     */
    @ExceptionHandler
    public Object exceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception exception) {
        LOGGER.error("统一异常处理：", exception);
        request.setAttribute("ex", exception);
        if (null != request.getHeader("X-Requested-With") && "XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"))) {
            request.setAttribute("requestHeader", "ajax");
        }
        // shiro没有权限异常
        if (exception instanceof UnauthorizedException) {
            return new BaseResult(BaseResultEnum.NOACCESS, "暂无权限");
        }
        // shiro会话已过期异常
        if (exception instanceof InvalidSessionException) {
            return new BaseResult(BaseResultEnum.NOEXPIRES, "会话已过期");
        }
        return new BaseResult(BaseResultEnum.ERROR,null);
    }
}
