package com.j4cf.common.base;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.session.InvalidSessionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: LongRou
 * @CreateDate: 2018 2018/3/28 18:18
 * @Version: 1.0
 **/
public class BaseApiController<T,S extends IService<T>> extends BaseController implements BaseApiService<T>{
    @Autowired
    protected S service;

    @Override
    public BaseResult insert(@RequestBody T entity) {
        if (service.insert(entity)){
            return new BaseResult(BaseResultEnum.SUCCESS,"添加成功！");
        }else {
            return new BaseResult(BaseResultEnum.ERROR,"添加失败！");
        }
    }

    @Override
    public BaseResult deleteById(String id) {
        try {
            try {
                int idx = Integer.parseInt(id);
                if (service.deleteById(idx)){return new BaseResult(BaseResultEnum.SUCCESS,"数据删除成功");}
                else {return new BaseResult(BaseResultEnum.ERROR,"删除失败");}
            }catch (NumberFormatException e){
                if (service.deleteById(id)){return new BaseResult(BaseResultEnum.SUCCESS,"数据删除成功");}
                else {return new BaseResult(BaseResultEnum.ERROR,"删除失败");}
            }
        }catch (Exception e){
            return new BaseResult(BaseResultEnum.ERROR,"删除失败,已使用的数据无法删除！");
        }
    }

    @Override
    public BaseResult updateById(@RequestBody T entity) {
        System.out.println("updateById => "+entity);
        if (service.updateById(entity)){return new BaseResult(BaseResultEnum.SUCCESS,"数据修改成功");}
        else {return new BaseResult(BaseResultEnum.ERROR,"修改失败");}
    }

    @Override
    public BaseResult<T> selectById(String id) {
        return new BaseResult(BaseResultEnum.SUCCESS,service.selectById(id));
    }


    @Override
    public BaseResult<List<T>> selectAll(@RequestParam Map<String, Object> params) {
        System.out.println("selectAll => "+params);
        BaseQuery BaseQuery = new BaseQuery<T>(params);
        try {
            return new BaseResult(BaseResultEnum.SUCCESS,service.selectList(BaseQuery.getEntityWrapper()));
        }catch (Exception e){
            return new BaseResult(BaseResultEnum.ERROR,"请检查参数是否正确");
        }
    }

    @Override
    public BaseResult<Page<T>> selectPage(@RequestParam Map<String, Object> params) {
        System.out.println("selectPage => "+params);
        BaseQuery BaseQuery = new BaseQuery<T>(params);
        try {
            Page<T> tPage =new Page<>(BaseQuery.getPage(),BaseQuery.getSize());
            tPage.setSearchCount(true);
            return new BaseResult(BaseResultEnum.SUCCESS,service.selectPage(tPage,BaseQuery.getEntityWrapper()));
        }catch (Exception e){
            return new BaseResult(BaseResultEnum.ERROR,"请检查参数是否正确");
        }
    }
    

}
