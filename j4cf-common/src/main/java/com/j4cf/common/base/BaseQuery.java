package com.j4cf.common.base;

import com.baomidou.mybatisplus.mapper.EntityWrapper;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Description: 基础查询参数
 * @Author: LongRou
 * @CreateDate: 2018-05-09 18:49
 * @Version: 1.0
 **/
public class BaseQuery<T> extends LinkedHashMap<String, Object> {
    private static final long serialVersionUID = 1L;
    //当前页码
    private int page = 1;
    //每页条数
    private int size = 10;

    private EntityWrapper<T> entityWrapper = new EntityWrapper<>();

    public BaseQuery(Map<String, Object> params){
        if (null != params){
            this.putAll(params);
            //分页参数
            if(params.get("page")!=null) {
                this.page = Integer.parseInt(params.get("page").toString());
            }
            if(params.get("size")!=null) {
                this.size = Integer.parseInt(params.get("size").toString());
            }
            if(params.get("orderBy")!=null) {
                entityWrapper.orderBy(params.get("orderBy").toString(),false);
            }
            if(params.get("orderByAsc")!=null) {
                entityWrapper.orderBy(params.get("orderByAsc").toString(),true);
            }
            this.remove("page");
            this.remove("size");
            this.remove("orderBy");
            this.remove("orderByAsc");
            this.forEach((k,v) ->{
                if (k.startsWith("_")){
                    entityWrapper.like(k.substring(1),v+"");
                }else {
                    entityWrapper.eq(k,v);
                }
            });
        }
    }


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public EntityWrapper<T> getEntityWrapper() {
        return entityWrapper;
    }

    public void setEntityWrapper(EntityWrapper<T> entityWrapper) {
        this.entityWrapper = entityWrapper;
    }
}